package main

import (

	// standard

	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"

	// internal

	"gitlab.com/zambiorix/go-wasm-dom/code-generation/generate"
)

// main
//
func main() {

	log.Println("go-wasm-dom : code generation")

	log.Println()

	dir, err := os.Getwd()

	if err != nil {

		log.Fatal(err)
	}

	updatePath := path.Join(dir, "code-generation", "generate-data-updated.json")

	inputPath := path.Join(dir, "code-generation", "generate-data.json")

	data, err := ioutil.ReadFile(inputPath)

	if err != nil {

		log.Fatal(err)
	}

	var all generate.All

	err = json.Unmarshal(data, &all)

	if err != nil {

		log.Fatal(err)
	}

	file, err := os.Create(path.Join(dir, "objects", "generated.go"))

	if err != nil {

		log.Fatal(err)
	}

	defer file.Close()

	all.Generate(file)

	update(&all, updatePath)

	log.Println()

	log.Println("finished")

	log.Println()

	log.Println(fileChecksum(inputPath))

	log.Println(fileChecksum(updatePath))
}

// update
//
func update(input *generate.All, path string) {

	if data, err := json.MarshalIndent(&input, "", "  "); err == nil {

		checksum := fmt.Sprintf("%x", sha256.Sum256(data))

		if checksum != fileChecksum(path) {

			ioutil.WriteFile(path, data, 0644)

			log.Println()

			log.Println("json data updated")
		}
	}
}

// fileChecksum
//
func fileChecksum(path string) string {

	file, err := os.Open(path)

	if err != nil {

		return ""
	}

	defer file.Close()

	checksum := sha256.New()

	if _, err := io.Copy(checksum, file); err != nil {

		return ""
	}

	return fmt.Sprintf("%x", checksum.Sum(nil))
}
