package generate

import (

	// standard

	// internal

	"gitlab.com/zambiorix/go-wasm-dom/code-generation/generate/utils"
)

// Argument ...
//
type Argument struct {

	// publics - unmarshalled

	Method *Method `json:"-"`

	// publics - marshalled

	Name string `json:"name"`

	Type *Type `json:"type"`

	IsVariadic bool `json:"isVariadic"`
}

// Initialize ...
//
func (argument *Argument) Initialize() {

}

// Generate ...
//
func (argument *Argument) Generate(writer utils.StringWriter) {

}
