package generate

import (

	// standard

	"fmt"
	"log"
	"strings"

	// internal

	"gitlab.com/zambiorix/go-wasm-dom/code-generation/generate/utils"
)

// MethodMap ...
//
type MethodMap map[string]*Method

// MarshalJSON ...
//
func (methodsMap MethodMap) MarshalJSON() ([]byte, error) {

	return utils.SortedMapMarshalJSON(methodsMap)
}

// SortedKeys ...
//
func (methodsMap MethodMap) SortedKeys() []string {

	return utils.SortedMapKeys(methodsMap)
}

// Methods ...
//
type Methods struct {

	// publics - unmarshalled

	Object *Object `json:"-"`

	// publics - marshalled

	Items MethodMap `json:"items"`
}

// HasItems ...
//
func (methods *Methods) HasItems() bool {

	return methods.Items != nil && len(methods.Items) > 0
}

// Initialize ...
//
func (methods *Methods) Initialize() {

	if methods.HasItems() {

		for _, key := range methods.Items.SortedKeys() {

			method := methods.Items[key]

			if method != nil {

				method.Methods = methods

				method.RawName = key

				if len(method.Name) == 0 {

					method.Name = strings.Title(method.RawName)
				}

				method.Initialize()
			}
		}
	}
}

// Generate ...
//
func (methods *Methods) Generate(writer utils.StringWriter) {

	if methods.HasItems() {

		if !methods.Object.Properties.HasProperties() {

			log.Println()
		}

		for _, key := range methods.Items.SortedKeys() {

			method := methods.Items[key]

			if method != nil {

				method.Generate(writer)
			}
		}

		log.Println()
	}
}

// GeneratePrototypes ...
//
func (methods *Methods) GeneratePrototypes(writer utils.StringWriter) {

	if methods.HasItems() {

		first := true

		for _, key := range methods.Items.SortedKeys() {

			method := methods.Items[key]

			if method != nil && method.CanCreate() {

				if first {

					first = false

					writer.WriteString("\n\n\t// methods\n")
				}

				writer.WriteString(fmt.Sprintf("\n\t%s", method.MethodPrototype()))
			}
		}
	}
}
