package generate

import (

	// standard

	"fmt"
	"log"

	// internal

	"gitlab.com/zambiorix/go-wasm-dom/code-generation/generate/utils"
)

// Property ...
//
type Property struct {

	// publics - unmarshalled

	Properties *Properties `json:"-"`

	RawName string `json:"-"`

	GetterName string `json:"-"`

	SetterName string `json:"-"`

	// publics - marshalled

	IsNew bool `json:"isNew"`

	IsObsolete bool `json:"isObsolete"`

	IsDeprecated bool `json:"isDeprecated"`

	IsNonStandard bool `json:"isNonStandard"`

	URL string `json:"url"`

	Name string `json:"name"`

	Type *Type `json:"type"`

	IsReadOnly bool `json:"isReadOnly"`
}

// Initialize ...
//
func (property *Property) Initialize() {

}

// CanCreate ...
//
func (property *Property) CanCreate() bool {

	if property.IsObsolete || property.IsDeprecated || property.IsNonStandard {

		return false
	}

	return property.Type != nil
}

// Generate ...
//
func (property *Property) Generate(writer utils.StringWriter) {

	if !property.CanCreate() {

		return
	}

	log.Println("-----> property :", property.GetterName)

	// template

	const templateGetter = `

// <URL>
//
func (<struct> *<STRUCT>) <METHOD>() <TYPE> {

	return nil
}`

	const templateSetter = `

// <URL>
//
func (<struct> *<STRUCT>) <METHOD>(value <TYPE>) {

	//
}`

	// getter

	utils.ParseTemplate(writer, templateGetter, map[string]interface{}{

		"<URL>": property.URL,

		"<struct>": property.Properties.Object.StructAbbreviation,

		"<STRUCT>": property.Properties.Object.StructName,

		"<METHOD>": property.GetterName,

		"<TYPE>": property.Type.Name,
	})

	// setter

	if property.IsReadOnly {

		return
	}

	utils.ParseTemplate(writer, templateSetter, map[string]interface{}{

		"<URL>": property.URL,

		"<struct>": property.Properties.Object.StructAbbreviation,

		"<STRUCT>": property.Properties.Object.StructName,

		"<METHOD>": property.SetterName,

		"<TYPE>": property.Type.Name,
	})
}

// GetterPrototype ...
//
func (property *Property) GetterPrototype() string {

	// TODO correct property type

	return fmt.Sprintf("%s() %s", property.GetterName, property.Type.Name)
}

// SetterPrototype ...
//
func (property *Property) SetterPrototype() string {

	// TODO correct property type

	return fmt.Sprintf("%s(value %s)", property.SetterName, property.Type.Name)
}
