package generate

import (

	// standard

	"fmt"
	"log"

	// internal

	"gitlab.com/zambiorix/go-wasm-dom/code-generation/generate/utils"
)

// Method ...
//
type Method struct {

	// publics - unmarshalled

	Methods *Methods `json:"-"`

	RawName string `json:"-"`

	// publics - marshalled

	IsNew bool `json:"isNew"`

	IsObsolete bool `json:"isObsolete"`

	IsDeprecated bool `json:"isDeprecated"`

	IsNonStandard bool `json:"isNonStandard"`

	URL string `json:"url"`

	Name string `json:"name"`

	Arguments []*Argument `json:"arguments"`

	Type *Type `json:"type"`
}

// Initialize ...
//
func (method *Method) Initialize() {

	if method.Arguments == nil {

		method.Arguments = make([]*Argument, 0)
	}

	for _, argument := range method.Arguments {

		if argument != nil {

			argument.Method = method

			argument.Initialize()
		}
	}
}

// Generate ...
//
func (method *Method) Generate(writer utils.StringWriter) {

	if !method.CanCreate() {

		return
	}

	log.Println("-------> method :", method.Name)

	// template

	const template = `

// <URL>
//
func (<struct> *<STRUCT>) <METHOD>() {

	<struct>.<BASETYPE>.Call("<NAME>")
}`

	// arguments

	for _, argument := range method.Arguments {

		if argument != nil {

			argument.Generate(writer)
		}
	}

	// output

	utils.ParseTemplate(writer, template, map[string]interface{}{

		"<URL>": method.URL,

		"<struct>": method.Methods.Object.StructAbbreviation,

		"<STRUCT>": method.Methods.Object.StructName,

		"<METHOD>": method.Name,

		"<BASETYPE>": baseInterface,

		"<NAME>": method.RawName,
	})
}

// CanCreate ...
//
func (method *Method) CanCreate() bool {

	if method.IsObsolete || method.IsDeprecated || method.IsNonStandard {

		return false
	}

	return true
}

// MethodPrototype ...
//
func (method *Method) MethodPrototype() string {
	/*
		if len(method.ReturnType) > 0 {

			return fmt.Sprintf("%s() %s", method.MethodName(), method.ReturnType)
		}
	*/
	return fmt.Sprintf("%s()", method.Name)
}
