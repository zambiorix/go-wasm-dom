package generate

import (

	// standard

	// internal

	"gitlab.com/zambiorix/go-wasm-dom/code-generation/generate/utils"
)

// baseInterface
//
const baseInterface = "JSValue"

// objectsCache
//
var objectsCache ObjectMap

// All ...
//
type All map[string]*Objects

// MarshalJSON ...
//
func (all All) MarshalJSON() ([]byte, error) {

	return utils.SortedMapMarshalJSON(all)
}

// SortedKeys ...
//
func (all All) SortedKeys() []string {

	return utils.SortedMapKeys(all)
}

// Generate ...
//
func (all All) Generate(writer utils.StringWriter) {

	// initialize

	objectsCache = make(ObjectMap)

	for _, key := range all.SortedKeys() {

		objects := all[key]

		if objects != nil {

			objects.All = all

			objects.RawName = key

			objects.Initialize()
		}
	}

	// generate header

	all.generateHeader(writer)

	// generate all

	for _, key := range all.SortedKeys() {

		objects := all[key]

		if objects != nil {

			objects.Generate(writer)
		}
	}
}

func (all All) generateHeader(writer utils.StringWriter) {

	writer.WriteString(`package objects

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!! this code is automatically generated !!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!`)

}
