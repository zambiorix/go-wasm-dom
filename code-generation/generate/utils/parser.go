package utils

import (

	// standard

	"fmt"
	"strings"
)

// StringWriter ...
//
type StringWriter interface {

	// prototypes

	WriteString(s string) (n int, err error)
}

// ParseTemplate ...
//
func ParseTemplate(writer StringWriter, template string, parameters map[string]interface{}) {

	if writer == nil {

		return
	}

	if parameters != nil {

		length := len(template)

		indexLast := 0

		indexCurrent := 0

		for indexCurrent <= length {

			for key := range parameters {

				if strings.HasPrefix(template[indexCurrent:], key) {

					if indexCurrent > indexLast {

						writer.WriteString(template[indexLast:indexCurrent])
					}

					value := parameters[key]

					if callback, ok := value.(func(StringWriter)); ok {

						callback(writer)

					} else {

						writer.WriteString(fmt.Sprint(value))
					}

					indexLast = indexCurrent + len(key)

					indexCurrent = indexLast - 1

					break
				}
			}

			indexCurrent++
		}

		writer.WriteString(template[indexLast:])
	}
}
