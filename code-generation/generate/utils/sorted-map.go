package utils

import (

	// standard

	"bytes"
	"encoding/json"
	"errors"
	"reflect"
	"sort"
)

// SortedMapKeys
//
func SortedMapKeys(items interface{}) []string {

	if items == nil {

		return nil
	}

	itemsMap := reflect.ValueOf(items)

	if itemsMap.Kind() != reflect.Map {

		return nil

	} else {

		mapKeys := itemsMap.MapKeys()

		sortedKeys := make([]string, len(mapKeys))

		index := 0

		for _, mapKey := range mapKeys {

			sortedKeys[index] = mapKey.String()

			index++
		}

		sort.Strings(sortedKeys)

		return sortedKeys
	}
}

// SortedMapMarshalJSON
//
func SortedMapMarshalJSON(items interface{}) ([]byte, error) {

	var buffer bytes.Buffer

	if items == nil {

		buffer.WriteString("null")

	} else {

		itemsMap := reflect.ValueOf(items)

		if itemsMap.Kind() != reflect.Map {

			return nil, errors.New("map argument required")

		} else {

			mapKeys := itemsMap.MapKeys()

			sort.Slice(mapKeys, func(i int, j int) bool {

				return mapKeys[i].String() < mapKeys[j].String()
			})

			buffer.WriteString("{")

			first := true

			for _, mapKey := range mapKeys {

				if first {

					first = false

				} else {

					buffer.WriteString(",")
				}

				buffer.WriteString(`"` + mapKey.String() + `":`)

				if data, err := json.Marshal(itemsMap.MapIndex(mapKey).Interface()); err == nil {

					buffer.Write(data)

				} else {

					return nil, err
				}
			}

			buffer.WriteString("}")
		}
	}

	return buffer.Bytes(), nil
}
