package generate

import (

	// standard

	"fmt"
	"log"

	// internal

	"gitlab.com/zambiorix/go-wasm-dom/code-generation/generate/utils"
)

// Object ...
//
type Object struct {

	// publics - unmarshalled

	Objects *Objects `json:"-"`

	InterfaceName string `json:"-"`

	StructName string `json:"-"`

	StructAbbreviation string `json:"-"`

	// publics - marshalled

	IsNew bool `json:"isNew"`

	IsObsolete bool `json:"isObsolete"`

	IsDeprecated bool `json:"isDeprecated"`

	IsNonStandard bool `json:"isNonStandard"`

	URL string `json:"url"`

	Type *Type `json:"type"`

	Properties *Properties `json:"properties"`

	Methods *Methods `json:"methods"`
}

// Initialize ...
//
func (object *Object) Initialize() {

	// cache

	objectsCache[object.InterfaceName] = object

	// properties

	if object.Properties == nil {

		object.Properties = new(Properties)
	}

	object.Properties.Object = object

	object.Properties.Initialize()

	// methods

	if object.Methods == nil {

		object.Methods = new(Methods)
	}

	object.Methods.Object = object

	object.Methods.Initialize()
}

// CanGenerate ...
//
func (object *Object) CanGenerate() bool {

	if object.IsObsolete || object.IsDeprecated || object.IsNonStandard {

		return false
	}

	return object.Type != nil
}

// Generate ...
//
func (object *Object) Generate(writer utils.StringWriter) {

	if !object.CanGenerate() {

		// TODO report

		return
	}

	log.Println("generate object :", object.InterfaceName)

	// interface, struct & constructors

	object.generateInterface(writer)

	object.generateStruct(writer)

	object.generateConstructor(writer)

	// properties & methods

	object.Properties.Generate(writer)

	object.Methods.Generate(writer)
}

// generateInterface
//
func (object *Object) generateInterface(writer utils.StringWriter) {

	const template = `

// <URL>
//
type <INTERFACE> interface {

	// inherited

	<TYPE>

	// make unique interface

	Unique<INTERFACE>Interface()<PROTOTYPES>
}`

	utils.ParseTemplate(writer, template, map[string]interface{}{

		"<URL>": object.URL,

		"<INTERFACE>": object.InterfaceName,

		"<TYPE>": object.Type.Name,

		"<PROTOTYPES>": object.generatePrototypes,
	})
}

// generateStruct
//
func (object *Object) generateStruct(writer utils.StringWriter) {

	const template = `

// <STRUCT>
//
type <STRUCT> struct {

	// inherited

	<TYPE>
}`

	utils.ParseTemplate(writer, template, map[string]interface{}{

		"<STRUCT>": object.StructName,

		"<TYPE>": object.Type.Name,
	})
}

// generateConstructor
//
func (object *Object) generateConstructor(writer utils.StringWriter) {

	const template = `

// new<INTERFACE>
//
func new<INTERFACE>(base interface{}) <INTERFACE> {

	if obj, ok := base.(<TYPE>); ok {

		return &<STRUCT>{obj}

	} else {

		<CONSTRUCTOR>
	}
}

// Unique<INTERFACE>Interface
// make sure <INTERFACE> is a unique interface type
func (<struct> *<STRUCT>) Unique<INTERFACE>Interface() {}`

	utils.ParseTemplate(writer, template, map[string]interface{}{

		"<INTERFACE>": object.InterfaceName,

		"<TYPE>": object.Type.Name,

		"<STRUCT>": object.StructName,

		"<struct>": object.StructAbbreviation,

		"<CONSTRUCTOR>": func(writer utils.StringWriter) {

			if object.Type == nil {

				writer.WriteString(fmt.Sprintf("\t\treturn nil"))

			} else {

				// TODO correct object type

				writer.WriteString(fmt.Sprintf("\t\treturn &%s{new%s(base)}", object.StructName, object.Type.Name))
			}
		},
	})
}

// generatePrototypes
//
func (object *Object) generatePrototypes(writer utils.StringWriter) {

	object.Properties.GeneratePrototypes(writer)

	object.Methods.GeneratePrototypes(writer)
}
