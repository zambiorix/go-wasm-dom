package generate

import (

	// standard

	"fmt"
	"log"
	"strings"

	// internal

	"gitlab.com/zambiorix/go-wasm-dom/code-generation/generate/utils"
)

// PropertyMap ...
//
type PropertyMap map[string]*Property

// MarshalJSON ...
//
func (propertiesMap PropertyMap) MarshalJSON() ([]byte, error) {

	return utils.SortedMapMarshalJSON(propertiesMap)
}

// SortedKeys ...
//
func (propertiesMap PropertyMap) SortedKeys() []string {

	return utils.SortedMapKeys(propertiesMap)
}

// Properties ...
//
type Properties struct {

	// publics - unmarshalled

	Object *Object `json:"-"`

	// publics - marshalled

	Items PropertyMap `json:"items"`
}

// HasItems ...
//
func (properties *Properties) HasItems() bool {

	return properties.Items != nil && len(properties.Items) > 0
}

// Initialize ...
//
func (properties *Properties) Initialize() {

	if properties.HasItems() {

		for _, key := range properties.Items.SortedKeys() {

			property := properties.Items[key]

			if property != nil {

				property.Properties = properties

				property.RawName = key

				if len(property.Name) == 0 {

					property.Name = strings.Title(property.RawName)
				}

				property.GetterName = property.Name

				property.SetterName = "Set" + property.Name

				property.Initialize()
			}
		}
	}
}

// Generate ...
//
func (properties *Properties) Generate(writer utils.StringWriter) {

	if properties.HasItems() {

		log.Println()

		for _, key := range properties.Items.SortedKeys() {

			property := properties.Items[key]

			if property != nil {

				property.Generate(writer)
			}
		}

		log.Println()
	}
}

// GeneratePrototypes ...
//
func (properties *Properties) GeneratePrototypes(writer utils.StringWriter) {

	if properties.HasItems() {

		first := true

		for _, key := range properties.Items.SortedKeys() {

			property := properties.Items[key]

			if property != nil && property.CanCreate() {

				if first {

					first = false

					writer.WriteString("\n\n\t// properties\n")
				}

				writer.WriteString(fmt.Sprintf("\n\t%s", property.GetterPrototype()))

				if property.IsReadOnly {

					continue
				}

				writer.WriteString(fmt.Sprintf("\n\t%s", property.SetterPrototype()))
			}
		}
	}
}

// HasProperties ...
//
func (properties *Properties) HasProperties() bool {

	if properties.HasItems() {

		for _, key := range properties.Items.SortedKeys() {

			property := properties.Items[key]

			if property != nil && property.CanCreate() {

				return true
			}
		}
	}

	return false
}
