package generate

import (

	// standard

	// internal

	"strings"

	"gitlab.com/zambiorix/go-wasm-dom/code-generation/generate/utils"
)

// ObjectMap ...
//
type ObjectMap map[string]*Object

// MarshalJSON ...
//
func (objectsMap ObjectMap) MarshalJSON() ([]byte, error) {

	return utils.SortedMapMarshalJSON(objectsMap)
}

// SortedKeys ...
//
func (objectsMap ObjectMap) SortedKeys() []string {

	return utils.SortedMapKeys(objectsMap)
}

// Objects ...
//
type Objects struct {

	// publics - unmarshalled

	All All `json:"-"`

	RawName string `json:"-"`

	// publics - marshalled

	NameToUpper bool `json:"nameToUpper"`

	Items ObjectMap `json:"items"`
}

// HasItems ...
//
func (objects *Objects) HasItems() bool {

	return objects.Items != nil && len(objects.Items) > 0
}

// Initialize ...
//
func (objects *Objects) Initialize() {

	if objects.HasItems() {

		for _, key := range objects.Items.SortedKeys() {

			object := objects.Items[key]

			if object != nil {

				object.Objects = objects

				if objects.NameToUpper {

					object.InterfaceName = strings.ToUpper(key)

				} else {

					object.InterfaceName = strings.Title(key)
				}

				object.StructName = "obj" + object.InterfaceName

				object.StructAbbreviation = "o"

				object.Initialize()
			}
		}
	}
}

// Generate ...
//
func (objects *Objects) Generate(writer utils.StringWriter) {

	if objects.HasItems() {

		for _, key := range objects.Items.SortedKeys() {

			object := objects.Items[key]

			if object != nil {

				object.Generate(writer)
			}
		}
	}
}
