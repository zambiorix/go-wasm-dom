package generate

// Type ...
//
type Type struct {

	// publics - unmarshalled

	// publics - marshalled

	Name string `json:"name"`

	IsPointer bool `json:"isPointer"`

	IsArray bool `json:"isArray"`
}
