package application

import (

	// standard

	"strconv"
	"time"

	// dom

	DOM "gitlab.com/zambiorix/go-wasm-dom"

	DOMElements "gitlab.com/zambiorix/go-wasm-dom/objects/elements"
)

// Application
//
type Application struct {

	// inherited

	*DOMElements.Body

	// private

	terminate chan bool

	terminated chan bool
}

// FromID

func FromID(id string) *Application {

	body := DOM.Document.GetElementByIDBody(id)

	if body == nil {

		return nil
	}

	application := new(Application)

	application.Body = body

	application.terminate = make(chan bool, 1)

	application.terminated = make(chan bool, 1)

	// DOM.Window.Alert("test")

	DOM.Console.Log("Console        : " + DOM.Console.String())
	DOM.Console.Log("Document       : " + DOM.Document.String())
	DOM.Console.Log("History        : " + DOM.History.String())
	DOM.Console.Log("LocalStorage   : " + DOM.LocalStorage.String())
	DOM.Console.Log("Location       : " + DOM.Location.String())
	DOM.Console.Log("Navigator      : " + DOM.Navigator.String())
	DOM.Console.Log("Screen         : " + DOM.Screen.String())
	DOM.Console.Log("Window         : " + DOM.Window.String())
	DOM.Console.Log("SessionStorage : " + DOM.SessionStorage.String())

	// console
	/*
		DOM.Console.Assert(false, "this is a console assert")

		DOM.Console.Clear()

		DOM.Console.Count()

		DOM.Console.CountWithMessage("test")

		DOM.Console.Error("this is a console error")

		DOM.Console.Info("this is a console info")

		DOM.Console.Log("this is a console log")

		DOM.Console.Warn("this is a console warning")

		DOM.Console.Warn(true)

		DOM.Console.Warn(nil)

		DOM.Console.Warn(1.1)

		// DOM.Console.Warn(time.Now())

		DOM.Console.Warn(application.JSElement())
	*/
	// test

	h1 := DOM.Document.CreateElementH1()

	h1.SetID("test")

	h1.SetInnerText("this is a H1 test")

	h1.SetAccessKey("a")

	h1.AccessKey()

	application.AppendChild(h1)

	DOM.Console.Log(h1.InnerText())

	if h1 = DOM.Document.GetElementByIDH1("test"); h1 == nil {

		DOM.Console.Log("not found")

	} else {

		DOM.Console.Log(h1.InnerText())
	}

	// ticker

	go func() {

		DOM.Console.Log("Ticker Started")

		defer DOM.Console.Log("Ticker Terminated")

		tickerElem := DOM.Document.CreateElementH2()

		application.AppendChild(tickerElem)

		ticker := time.NewTicker(time.Millisecond * 1000)

		defer ticker.Stop()

		index := 0

		tickerElem.SetInnerText(strconv.Itoa(index))

		for {

			select {

			case <-ticker.C:

				index++

				tickerElem.SetInnerText(strconv.Itoa(index))

			case <-application.terminate:

				application.terminated <- true

				return
			}
		}

	}()

	return application
}

// Close
//
func (application *Application) Close() {

	DOM.Console.Log("Application Finalizing")

	application.terminate <- true

	<-application.terminated

	close(application.terminate)

	close(application.terminated)

	DOM.Console.Log("Application Finalized")
}
