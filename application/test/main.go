package main

import "fmt"

type foo struct{}

func (foo *foo) Test() {}

type bar struct{}

func main() {

	var x interface{} = &foo{}

	switch t := x.(type) {

	case foo:

		fmt.Println("foo")

	case bar:

		fmt.Println("bar")

	default:

		fmt.Println(t)
	}
}
