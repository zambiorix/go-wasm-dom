package objects

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!! this code is automatically generated !!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// https://developer.mozilla.org/en-US/docs/Web/API/Document
//
type Document interface {

	// inherited

	JSValue

	// make unique interface

	UniqueDocumentInterface()
}

// objDocument
//
type objDocument struct {

	// inherited

	JSValue
}

// newDocument
//
func newDocument(base interface{}) Document {

	if obj, ok := base.(JSValue); ok {

		return &objDocument{obj}

	} else {

		return &objDocument{newJSValue(base)}
	}
}

// UniqueDocumentInterface
// make sure Document is a unique interface type
func (o *objDocument) UniqueDocumentInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/Window
//
type Window interface {

	// inherited

	JSValue

	// make unique interface

	UniqueWindowInterface()

	// properties

	Console() Console
	Document() Document

	// methods

	Alert()
	Atob()
	Back()
	Blur()
	Btoa()
	CancelAnimationFrame()
	CancelIdleCallback()
	CaptureEvents()
	ClearImmediate()
	ClearInterval()
	ClearTimeout()
	Close()
	Confirm()
	ConvertPointFromNodeToPage()
	ConvertPointFromPageToNode()
	CreateImageBitmap()
	Dump()
	Event()
	Fetch()
	Find()
	Focus()
	Forward()
	GetAttention()
	GetComputedStyle()
	GetDefaultComputedStyle()
	GetSelection()
	Home()
	MatchMedia()
	Minimize()
	MoveBy()
	MoveTo()
	Open()
	OpenDialog()
	PostMessage()
	Print()
	Prompt()
	ReleaseEvents()
	RequestAnimationFrame()
	RequestFileSystem()
	RequestIdleCallback()
	ResizeBy()
	ResizeTo()
	Restore()
	RouteEvent()
	Scroll()
	ScrollBy()
	ScrollByLines()
	ScrollByPages()
	ScrollTo()
	SetCursor()
	SetImmediate()
	SetInterval()
	SetTimeout()
	ShowModalDialog()
	SizeToContent()
	Stop()
	UpdateCommands()
}

// objWindow
//
type objWindow struct {

	// inherited

	JSValue
}

// newWindow
//
func newWindow(base interface{}) Window {

	if obj, ok := base.(JSValue); ok {

		return &objWindow{obj}

	} else {

		return &objWindow{newJSValue(base)}
	}
}

// UniqueWindowInterface
// make sure Window is a unique interface type
func (o *objWindow) UniqueWindowInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/Window/console
//
func (o *objWindow) Console() Console {

	return nil
}

// https://developer.mozilla.org/en-US/docs/Web/API/Window/document
//
func (o *objWindow) Document() Document {

	return nil
}

//
//
func (o *objWindow) Alert() {

	o.JSValue.Call("alert")
}

//
//
func (o *objWindow) Atob() {

	o.JSValue.Call("atob")
}

//
//
func (o *objWindow) Back() {

	o.JSValue.Call("back")
}

//
//
func (o *objWindow) Blur() {

	o.JSValue.Call("blur")
}

//
//
func (o *objWindow) Btoa() {

	o.JSValue.Call("btoa")
}

//
//
func (o *objWindow) CancelAnimationFrame() {

	o.JSValue.Call("cancelAnimationFrame")
}

//
//
func (o *objWindow) CancelIdleCallback() {

	o.JSValue.Call("cancelIdleCallback")
}

//
//
func (o *objWindow) CaptureEvents() {

	o.JSValue.Call("captureEvents")
}

//
//
func (o *objWindow) ClearImmediate() {

	o.JSValue.Call("clearImmediate")
}

//
//
func (o *objWindow) ClearInterval() {

	o.JSValue.Call("clearInterval")
}

//
//
func (o *objWindow) ClearTimeout() {

	o.JSValue.Call("clearTimeout")
}

//
//
func (o *objWindow) Close() {

	o.JSValue.Call("close")
}

//
//
func (o *objWindow) Confirm() {

	o.JSValue.Call("confirm")
}

//
//
func (o *objWindow) ConvertPointFromNodeToPage() {

	o.JSValue.Call("convertPointFromNodeToPage")
}

//
//
func (o *objWindow) ConvertPointFromPageToNode() {

	o.JSValue.Call("convertPointFromPageToNode")
}

//
//
func (o *objWindow) CreateImageBitmap() {

	o.JSValue.Call("createImageBitmap")
}

//
//
func (o *objWindow) Dump() {

	o.JSValue.Call("dump")
}

//
//
func (o *objWindow) Event() {

	o.JSValue.Call("event")
}

//
//
func (o *objWindow) Fetch() {

	o.JSValue.Call("fetch")
}

//
//
func (o *objWindow) Find() {

	o.JSValue.Call("find")
}

//
//
func (o *objWindow) Focus() {

	o.JSValue.Call("focus")
}

//
//
func (o *objWindow) Forward() {

	o.JSValue.Call("forward")
}

//
//
func (o *objWindow) GetAttention() {

	o.JSValue.Call("getAttention")
}

//
//
func (o *objWindow) GetComputedStyle() {

	o.JSValue.Call("getComputedStyle")
}

//
//
func (o *objWindow) GetDefaultComputedStyle() {

	o.JSValue.Call("getDefaultComputedStyle")
}

//
//
func (o *objWindow) GetSelection() {

	o.JSValue.Call("getSelection")
}

//
//
func (o *objWindow) Home() {

	o.JSValue.Call("home")
}

//
//
func (o *objWindow) MatchMedia() {

	o.JSValue.Call("matchMedia")
}

//
//
func (o *objWindow) Minimize() {

	o.JSValue.Call("minimize")
}

//
//
func (o *objWindow) MoveBy() {

	o.JSValue.Call("moveBy")
}

//
//
func (o *objWindow) MoveTo() {

	o.JSValue.Call("moveTo")
}

//
//
func (o *objWindow) Open() {

	o.JSValue.Call("open")
}

//
//
func (o *objWindow) OpenDialog() {

	o.JSValue.Call("openDialog")
}

//
//
func (o *objWindow) PostMessage() {

	o.JSValue.Call("postMessage")
}

//
//
func (o *objWindow) Print() {

	o.JSValue.Call("print")
}

//
//
func (o *objWindow) Prompt() {

	o.JSValue.Call("prompt")
}

//
//
func (o *objWindow) ReleaseEvents() {

	o.JSValue.Call("releaseEvents")
}

//
//
func (o *objWindow) RequestAnimationFrame() {

	o.JSValue.Call("requestAnimationFrame")
}

//
//
func (o *objWindow) RequestFileSystem() {

	o.JSValue.Call("requestFileSystem")
}

//
//
func (o *objWindow) RequestIdleCallback() {

	o.JSValue.Call("requestIdleCallback")
}

//
//
func (o *objWindow) ResizeBy() {

	o.JSValue.Call("resizeBy")
}

//
//
func (o *objWindow) ResizeTo() {

	o.JSValue.Call("resizeTo")
}

//
//
func (o *objWindow) Restore() {

	o.JSValue.Call("restore")
}

//
//
func (o *objWindow) RouteEvent() {

	o.JSValue.Call("routeEvent")
}

//
//
func (o *objWindow) Scroll() {

	o.JSValue.Call("scroll")
}

//
//
func (o *objWindow) ScrollBy() {

	o.JSValue.Call("scrollBy")
}

//
//
func (o *objWindow) ScrollByLines() {

	o.JSValue.Call("scrollByLines")
}

//
//
func (o *objWindow) ScrollByPages() {

	o.JSValue.Call("scrollByPages")
}

//
//
func (o *objWindow) ScrollTo() {

	o.JSValue.Call("scrollTo")
}

//
//
func (o *objWindow) SetCursor() {

	o.JSValue.Call("setCursor")
}

//
//
func (o *objWindow) SetImmediate() {

	o.JSValue.Call("setImmediate")
}

//
//
func (o *objWindow) SetInterval() {

	o.JSValue.Call("setInterval")
}

//
//
func (o *objWindow) SetTimeout() {

	o.JSValue.Call("setTimeout")
}

//
//
func (o *objWindow) ShowModalDialog() {

	o.JSValue.Call("showModalDialog")
}

//
//
func (o *objWindow) SizeToContent() {

	o.JSValue.Call("sizeToContent")
}

//
//
func (o *objWindow) Stop() {

	o.JSValue.Call("stop")
}

//
//
func (o *objWindow) UpdateCommands() {

	o.JSValue.Call("updateCommands")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console
//
type Console interface {

	// inherited

	JSValue

	// make unique interface

	UniqueConsoleInterface()

	// methods

	Assert()
	Clear()
	Count()
	CountReset()
	Dir()
	DirXML()
	Error()
	Group()
	GroupCollapsed()
	GroupEnd()
	Info()
	Log()
	Profile()
	ProfileEnd()
	Table()
	Time()
	TimeEnd()
	TimeLog()
	TimeStamp()
	Trace()
	Warn()
}

// objConsole
//
type objConsole struct {

	// inherited

	JSValue
}

// newConsole
//
func newConsole(base interface{}) Console {

	if obj, ok := base.(JSValue); ok {

		return &objConsole{obj}

	} else {

		return &objConsole{newJSValue(base)}
	}
}

// UniqueConsoleInterface
// make sure Console is a unique interface type
func (o *objConsole) UniqueConsoleInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/console/assert
//
func (o *objConsole) Assert() {

	o.JSValue.Call("assert")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/clear
//
func (o *objConsole) Clear() {

	o.JSValue.Call("clear")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/count
//
func (o *objConsole) Count() {

	o.JSValue.Call("count")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/countReset
//
func (o *objConsole) CountReset() {

	o.JSValue.Call("countReset")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/dir
//
func (o *objConsole) Dir() {

	o.JSValue.Call("dir")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/dirxml
//
func (o *objConsole) DirXML() {

	o.JSValue.Call("dirxml")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/error
//
func (o *objConsole) Error() {

	o.JSValue.Call("error")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/group
//
func (o *objConsole) Group() {

	o.JSValue.Call("group")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/groupCollapsed
//
func (o *objConsole) GroupCollapsed() {

	o.JSValue.Call("groupCollapsed")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/groupEnd
//
func (o *objConsole) GroupEnd() {

	o.JSValue.Call("groupEnd")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/info
//
func (o *objConsole) Info() {

	o.JSValue.Call("info")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/log
//
func (o *objConsole) Log() {

	o.JSValue.Call("log")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/profile
//
func (o *objConsole) Profile() {

	o.JSValue.Call("profile")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/profileEnd
//
func (o *objConsole) ProfileEnd() {

	o.JSValue.Call("profileEnd")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/table
//
func (o *objConsole) Table() {

	o.JSValue.Call("table")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/time
//
func (o *objConsole) Time() {

	o.JSValue.Call("time")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/timeEnd
//
func (o *objConsole) TimeEnd() {

	o.JSValue.Call("timeEnd")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/timeLog
//
func (o *objConsole) TimeLog() {

	o.JSValue.Call("timeLog")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/timeStamp
//
func (o *objConsole) TimeStamp() {

	o.JSValue.Call("timeStamp")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/trace
//
func (o *objConsole) Trace() {

	o.JSValue.Call("trace")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Console/warn
//
func (o *objConsole) Warn() {

	o.JSValue.Call("warn")
}

// https://developer.mozilla.org/en-US/docs/Web/API/Element
//
type Element interface {

	// inherited

	Node

	// make unique interface

	UniqueElementInterface()
}

// objElement
//
type objElement struct {

	// inherited

	Node
}

// newElement
//
func newElement(base interface{}) Element {

	if obj, ok := base.(Node); ok {

		return &objElement{obj}

	} else {

		return &objElement{newNode(base)}
	}
}

// UniqueElementInterface
// make sure Element is a unique interface type
func (o *objElement) UniqueElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/EventTarget
//
type EventTarget interface {

	// inherited

	JSValue

	// make unique interface

	UniqueEventTargetInterface()
}

// objEventTarget
//
type objEventTarget struct {

	// inherited

	JSValue
}

// newEventTarget
//
func newEventTarget(base interface{}) EventTarget {

	if obj, ok := base.(JSValue); ok {

		return &objEventTarget{obj}

	} else {

		return &objEventTarget{newJSValue(base)}
	}
}

// UniqueEventTargetInterface
// make sure EventTarget is a unique interface type
func (o *objEventTarget) UniqueEventTargetInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/Node
//
type Node interface {

	// inherited

	EventTarget

	// make unique interface

	UniqueNodeInterface()
}

// objNode
//
type objNode struct {

	// inherited

	EventTarget
}

// newNode
//
func newNode(base interface{}) Node {

	if obj, ok := base.(EventTarget); ok {

		return &objNode{obj}

	} else {

		return &objNode{newEventTarget(base)}
	}
}

// UniqueNodeInterface
// make sure Node is a unique interface type
func (o *objNode) UniqueNodeInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a
//
type A interface {

	// inherited

	HTMLAnchorElement

	// make unique interface

	UniqueAInterface()
}

// objA
//
type objA struct {

	// inherited

	HTMLAnchorElement
}

// newA
//
func newA(base interface{}) A {

	if obj, ok := base.(HTMLAnchorElement); ok {

		return &objA{obj}

	} else {

		return &objA{newHTMLAnchorElement(base)}
	}
}

// UniqueAInterface
// make sure A is a unique interface type
func (o *objA) UniqueAInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/abbr
//
type ABBR interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueABBRInterface()
}

// objABBR
//
type objABBR struct {

	// inherited

	HTMLElement
}

// newABBR
//
func newABBR(base interface{}) ABBR {

	if obj, ok := base.(HTMLElement); ok {

		return &objABBR{obj}

	} else {

		return &objABBR{newHTMLElement(base)}
	}
}

// UniqueABBRInterface
// make sure ABBR is a unique interface type
func (o *objABBR) UniqueABBRInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/address
//
type ADDRESS interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueADDRESSInterface()
}

// objADDRESS
//
type objADDRESS struct {

	// inherited

	HTMLElement
}

// newADDRESS
//
func newADDRESS(base interface{}) ADDRESS {

	if obj, ok := base.(HTMLElement); ok {

		return &objADDRESS{obj}

	} else {

		return &objADDRESS{newHTMLElement(base)}
	}
}

// UniqueADDRESSInterface
// make sure ADDRESS is a unique interface type
func (o *objADDRESS) UniqueADDRESSInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/area
//
type AREA interface {

	// inherited

	HTMLAreaElement

	// make unique interface

	UniqueAREAInterface()
}

// objAREA
//
type objAREA struct {

	// inherited

	HTMLAreaElement
}

// newAREA
//
func newAREA(base interface{}) AREA {

	if obj, ok := base.(HTMLAreaElement); ok {

		return &objAREA{obj}

	} else {

		return &objAREA{newHTMLAreaElement(base)}
	}
}

// UniqueAREAInterface
// make sure AREA is a unique interface type
func (o *objAREA) UniqueAREAInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/article
//
type ARTICLE interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueARTICLEInterface()
}

// objARTICLE
//
type objARTICLE struct {

	// inherited

	HTMLElement
}

// newARTICLE
//
func newARTICLE(base interface{}) ARTICLE {

	if obj, ok := base.(HTMLElement); ok {

		return &objARTICLE{obj}

	} else {

		return &objARTICLE{newHTMLElement(base)}
	}
}

// UniqueARTICLEInterface
// make sure ARTICLE is a unique interface type
func (o *objARTICLE) UniqueARTICLEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/aside
//
type ASIDE interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueASIDEInterface()
}

// objASIDE
//
type objASIDE struct {

	// inherited

	HTMLElement
}

// newASIDE
//
func newASIDE(base interface{}) ASIDE {

	if obj, ok := base.(HTMLElement); ok {

		return &objASIDE{obj}

	} else {

		return &objASIDE{newHTMLElement(base)}
	}
}

// UniqueASIDEInterface
// make sure ASIDE is a unique interface type
func (o *objASIDE) UniqueASIDEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio
//
type AUDIO interface {

	// inherited

	HTMLAudioElement

	// make unique interface

	UniqueAUDIOInterface()
}

// objAUDIO
//
type objAUDIO struct {

	// inherited

	HTMLAudioElement
}

// newAUDIO
//
func newAUDIO(base interface{}) AUDIO {

	if obj, ok := base.(HTMLAudioElement); ok {

		return &objAUDIO{obj}

	} else {

		return &objAUDIO{newHTMLAudioElement(base)}
	}
}

// UniqueAUDIOInterface
// make sure AUDIO is a unique interface type
func (o *objAUDIO) UniqueAUDIOInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/b
//
type B interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueBInterface()
}

// objB
//
type objB struct {

	// inherited

	HTMLElement
}

// newB
//
func newB(base interface{}) B {

	if obj, ok := base.(HTMLElement); ok {

		return &objB{obj}

	} else {

		return &objB{newHTMLElement(base)}
	}
}

// UniqueBInterface
// make sure B is a unique interface type
func (o *objB) UniqueBInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/bdi
//
type BDI interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueBDIInterface()
}

// objBDI
//
type objBDI struct {

	// inherited

	HTMLElement
}

// newBDI
//
func newBDI(base interface{}) BDI {

	if obj, ok := base.(HTMLElement); ok {

		return &objBDI{obj}

	} else {

		return &objBDI{newHTMLElement(base)}
	}
}

// UniqueBDIInterface
// make sure BDI is a unique interface type
func (o *objBDI) UniqueBDIInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/bdo
//
type BDO interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueBDOInterface()
}

// objBDO
//
type objBDO struct {

	// inherited

	HTMLElement
}

// newBDO
//
func newBDO(base interface{}) BDO {

	if obj, ok := base.(HTMLElement); ok {

		return &objBDO{obj}

	} else {

		return &objBDO{newHTMLElement(base)}
	}
}

// UniqueBDOInterface
// make sure BDO is a unique interface type
func (o *objBDO) UniqueBDOInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/blockquote
//
type BLOCKQUOTE interface {

	// inherited

	HTMLQuoteElement

	// make unique interface

	UniqueBLOCKQUOTEInterface()
}

// objBLOCKQUOTE
//
type objBLOCKQUOTE struct {

	// inherited

	HTMLQuoteElement
}

// newBLOCKQUOTE
//
func newBLOCKQUOTE(base interface{}) BLOCKQUOTE {

	if obj, ok := base.(HTMLQuoteElement); ok {

		return &objBLOCKQUOTE{obj}

	} else {

		return &objBLOCKQUOTE{newHTMLQuoteElement(base)}
	}
}

// UniqueBLOCKQUOTEInterface
// make sure BLOCKQUOTE is a unique interface type
func (o *objBLOCKQUOTE) UniqueBLOCKQUOTEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/body
//
type BODY interface {

	// inherited

	HTMLBodyElement

	// make unique interface

	UniqueBODYInterface()
}

// objBODY
//
type objBODY struct {

	// inherited

	HTMLBodyElement
}

// newBODY
//
func newBODY(base interface{}) BODY {

	if obj, ok := base.(HTMLBodyElement); ok {

		return &objBODY{obj}

	} else {

		return &objBODY{newHTMLBodyElement(base)}
	}
}

// UniqueBODYInterface
// make sure BODY is a unique interface type
func (o *objBODY) UniqueBODYInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/br
//
type BR interface {

	// inherited

	HTMLBRElement

	// make unique interface

	UniqueBRInterface()
}

// objBR
//
type objBR struct {

	// inherited

	HTMLBRElement
}

// newBR
//
func newBR(base interface{}) BR {

	if obj, ok := base.(HTMLBRElement); ok {

		return &objBR{obj}

	} else {

		return &objBR{newHTMLBRElement(base)}
	}
}

// UniqueBRInterface
// make sure BR is a unique interface type
func (o *objBR) UniqueBRInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button
//
type BUTTON interface {

	// inherited

	HTMLButtonElement

	// make unique interface

	UniqueBUTTONInterface()
}

// objBUTTON
//
type objBUTTON struct {

	// inherited

	HTMLButtonElement
}

// newBUTTON
//
func newBUTTON(base interface{}) BUTTON {

	if obj, ok := base.(HTMLButtonElement); ok {

		return &objBUTTON{obj}

	} else {

		return &objBUTTON{newHTMLButtonElement(base)}
	}
}

// UniqueBUTTONInterface
// make sure BUTTON is a unique interface type
func (o *objBUTTON) UniqueBUTTONInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/canvas
//
type CANVAS interface {

	// inherited

	HTMLCanvasElement

	// make unique interface

	UniqueCANVASInterface()
}

// objCANVAS
//
type objCANVAS struct {

	// inherited

	HTMLCanvasElement
}

// newCANVAS
//
func newCANVAS(base interface{}) CANVAS {

	if obj, ok := base.(HTMLCanvasElement); ok {

		return &objCANVAS{obj}

	} else {

		return &objCANVAS{newHTMLCanvasElement(base)}
	}
}

// UniqueCANVASInterface
// make sure CANVAS is a unique interface type
func (o *objCANVAS) UniqueCANVASInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/caption
//
type CAPTION interface {

	// inherited

	HTMLTableCaptionElement

	// make unique interface

	UniqueCAPTIONInterface()
}

// objCAPTION
//
type objCAPTION struct {

	// inherited

	HTMLTableCaptionElement
}

// newCAPTION
//
func newCAPTION(base interface{}) CAPTION {

	if obj, ok := base.(HTMLTableCaptionElement); ok {

		return &objCAPTION{obj}

	} else {

		return &objCAPTION{newHTMLTableCaptionElement(base)}
	}
}

// UniqueCAPTIONInterface
// make sure CAPTION is a unique interface type
func (o *objCAPTION) UniqueCAPTIONInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/cite
//
type CITE interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueCITEInterface()
}

// objCITE
//
type objCITE struct {

	// inherited

	HTMLElement
}

// newCITE
//
func newCITE(base interface{}) CITE {

	if obj, ok := base.(HTMLElement); ok {

		return &objCITE{obj}

	} else {

		return &objCITE{newHTMLElement(base)}
	}
}

// UniqueCITEInterface
// make sure CITE is a unique interface type
func (o *objCITE) UniqueCITEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/code
//
type CODE interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueCODEInterface()
}

// objCODE
//
type objCODE struct {

	// inherited

	HTMLElement
}

// newCODE
//
func newCODE(base interface{}) CODE {

	if obj, ok := base.(HTMLElement); ok {

		return &objCODE{obj}

	} else {

		return &objCODE{newHTMLElement(base)}
	}
}

// UniqueCODEInterface
// make sure CODE is a unique interface type
func (o *objCODE) UniqueCODEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/col
//
type COL interface {

	// inherited

	HTMLTableColElement

	// make unique interface

	UniqueCOLInterface()
}

// objCOL
//
type objCOL struct {

	// inherited

	HTMLTableColElement
}

// newCOL
//
func newCOL(base interface{}) COL {

	if obj, ok := base.(HTMLTableColElement); ok {

		return &objCOL{obj}

	} else {

		return &objCOL{newHTMLTableColElement(base)}
	}
}

// UniqueCOLInterface
// make sure COL is a unique interface type
func (o *objCOL) UniqueCOLInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/colgroup
//
type COLGROUP interface {

	// inherited

	HTMLTableColElement

	// make unique interface

	UniqueCOLGROUPInterface()
}

// objCOLGROUP
//
type objCOLGROUP struct {

	// inherited

	HTMLTableColElement
}

// newCOLGROUP
//
func newCOLGROUP(base interface{}) COLGROUP {

	if obj, ok := base.(HTMLTableColElement); ok {

		return &objCOLGROUP{obj}

	} else {

		return &objCOLGROUP{newHTMLTableColElement(base)}
	}
}

// UniqueCOLGROUPInterface
// make sure COLGROUP is a unique interface type
func (o *objCOLGROUP) UniqueCOLGROUPInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/data
//
type DATA interface {

	// inherited

	HTMLDataElement

	// make unique interface

	UniqueDATAInterface()
}

// objDATA
//
type objDATA struct {

	// inherited

	HTMLDataElement
}

// newDATA
//
func newDATA(base interface{}) DATA {

	if obj, ok := base.(HTMLDataElement); ok {

		return &objDATA{obj}

	} else {

		return &objDATA{newHTMLDataElement(base)}
	}
}

// UniqueDATAInterface
// make sure DATA is a unique interface type
func (o *objDATA) UniqueDATAInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist
//
type DATALIST interface {

	// inherited

	HTMLDataListElement

	// make unique interface

	UniqueDATALISTInterface()
}

// objDATALIST
//
type objDATALIST struct {

	// inherited

	HTMLDataListElement
}

// newDATALIST
//
func newDATALIST(base interface{}) DATALIST {

	if obj, ok := base.(HTMLDataListElement); ok {

		return &objDATALIST{obj}

	} else {

		return &objDATALIST{newHTMLDataListElement(base)}
	}
}

// UniqueDATALISTInterface
// make sure DATALIST is a unique interface type
func (o *objDATALIST) UniqueDATALISTInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dd
//
type DD interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueDDInterface()
}

// objDD
//
type objDD struct {

	// inherited

	HTMLElement
}

// newDD
//
func newDD(base interface{}) DD {

	if obj, ok := base.(HTMLElement); ok {

		return &objDD{obj}

	} else {

		return &objDD{newHTMLElement(base)}
	}
}

// UniqueDDInterface
// make sure DD is a unique interface type
func (o *objDD) UniqueDDInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/del
//
type DEL interface {

	// inherited

	HTMLModElement

	// make unique interface

	UniqueDELInterface()
}

// objDEL
//
type objDEL struct {

	// inherited

	HTMLModElement
}

// newDEL
//
func newDEL(base interface{}) DEL {

	if obj, ok := base.(HTMLModElement); ok {

		return &objDEL{obj}

	} else {

		return &objDEL{newHTMLModElement(base)}
	}
}

// UniqueDELInterface
// make sure DEL is a unique interface type
func (o *objDEL) UniqueDELInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/details
//
type DETAILS interface {

	// inherited

	HTMLDetailsElement

	// make unique interface

	UniqueDETAILSInterface()
}

// objDETAILS
//
type objDETAILS struct {

	// inherited

	HTMLDetailsElement
}

// newDETAILS
//
func newDETAILS(base interface{}) DETAILS {

	if obj, ok := base.(HTMLDetailsElement); ok {

		return &objDETAILS{obj}

	} else {

		return &objDETAILS{newHTMLDetailsElement(base)}
	}
}

// UniqueDETAILSInterface
// make sure DETAILS is a unique interface type
func (o *objDETAILS) UniqueDETAILSInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dfn
//
type DFN interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueDFNInterface()
}

// objDFN
//
type objDFN struct {

	// inherited

	HTMLElement
}

// newDFN
//
func newDFN(base interface{}) DFN {

	if obj, ok := base.(HTMLElement); ok {

		return &objDFN{obj}

	} else {

		return &objDFN{newHTMLElement(base)}
	}
}

// UniqueDFNInterface
// make sure DFN is a unique interface type
func (o *objDFN) UniqueDFNInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dialog
//
type DIALOG interface {

	// inherited

	HTMLDialogElement

	// make unique interface

	UniqueDIALOGInterface()
}

// objDIALOG
//
type objDIALOG struct {

	// inherited

	HTMLDialogElement
}

// newDIALOG
//
func newDIALOG(base interface{}) DIALOG {

	if obj, ok := base.(HTMLDialogElement); ok {

		return &objDIALOG{obj}

	} else {

		return &objDIALOG{newHTMLDialogElement(base)}
	}
}

// UniqueDIALOGInterface
// make sure DIALOG is a unique interface type
func (o *objDIALOG) UniqueDIALOGInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/div
//
type DIV interface {

	// inherited

	HTMLDivElement

	// make unique interface

	UniqueDIVInterface()
}

// objDIV
//
type objDIV struct {

	// inherited

	HTMLDivElement
}

// newDIV
//
func newDIV(base interface{}) DIV {

	if obj, ok := base.(HTMLDivElement); ok {

		return &objDIV{obj}

	} else {

		return &objDIV{newHTMLDivElement(base)}
	}
}

// UniqueDIVInterface
// make sure DIV is a unique interface type
func (o *objDIV) UniqueDIVInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl
//
type DL interface {

	// inherited

	HTMLDListElement

	// make unique interface

	UniqueDLInterface()
}

// objDL
//
type objDL struct {

	// inherited

	HTMLDListElement
}

// newDL
//
func newDL(base interface{}) DL {

	if obj, ok := base.(HTMLDListElement); ok {

		return &objDL{obj}

	} else {

		return &objDL{newHTMLDListElement(base)}
	}
}

// UniqueDLInterface
// make sure DL is a unique interface type
func (o *objDL) UniqueDLInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dt
//
type DT interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueDTInterface()
}

// objDT
//
type objDT struct {

	// inherited

	HTMLElement
}

// newDT
//
func newDT(base interface{}) DT {

	if obj, ok := base.(HTMLElement); ok {

		return &objDT{obj}

	} else {

		return &objDT{newHTMLElement(base)}
	}
}

// UniqueDTInterface
// make sure DT is a unique interface type
func (o *objDT) UniqueDTInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/em
//
type EM interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueEMInterface()
}

// objEM
//
type objEM struct {

	// inherited

	HTMLElement
}

// newEM
//
func newEM(base interface{}) EM {

	if obj, ok := base.(HTMLElement); ok {

		return &objEM{obj}

	} else {

		return &objEM{newHTMLElement(base)}
	}
}

// UniqueEMInterface
// make sure EM is a unique interface type
func (o *objEM) UniqueEMInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/embed
//
type EMBED interface {

	// inherited

	HTMLEmbedElement

	// make unique interface

	UniqueEMBEDInterface()
}

// objEMBED
//
type objEMBED struct {

	// inherited

	HTMLEmbedElement
}

// newEMBED
//
func newEMBED(base interface{}) EMBED {

	if obj, ok := base.(HTMLEmbedElement); ok {

		return &objEMBED{obj}

	} else {

		return &objEMBED{newHTMLEmbedElement(base)}
	}
}

// UniqueEMBEDInterface
// make sure EMBED is a unique interface type
func (o *objEMBED) UniqueEMBEDInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/fieldset
//
type FIELDSET interface {

	// inherited

	HTMLFieldSetElement

	// make unique interface

	UniqueFIELDSETInterface()
}

// objFIELDSET
//
type objFIELDSET struct {

	// inherited

	HTMLFieldSetElement
}

// newFIELDSET
//
func newFIELDSET(base interface{}) FIELDSET {

	if obj, ok := base.(HTMLFieldSetElement); ok {

		return &objFIELDSET{obj}

	} else {

		return &objFIELDSET{newHTMLFieldSetElement(base)}
	}
}

// UniqueFIELDSETInterface
// make sure FIELDSET is a unique interface type
func (o *objFIELDSET) UniqueFIELDSETInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/figcaption
//
type FIGCAPTION interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueFIGCAPTIONInterface()
}

// objFIGCAPTION
//
type objFIGCAPTION struct {

	// inherited

	HTMLElement
}

// newFIGCAPTION
//
func newFIGCAPTION(base interface{}) FIGCAPTION {

	if obj, ok := base.(HTMLElement); ok {

		return &objFIGCAPTION{obj}

	} else {

		return &objFIGCAPTION{newHTMLElement(base)}
	}
}

// UniqueFIGCAPTIONInterface
// make sure FIGCAPTION is a unique interface type
func (o *objFIGCAPTION) UniqueFIGCAPTIONInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/figure
//
type FIGURE interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueFIGUREInterface()
}

// objFIGURE
//
type objFIGURE struct {

	// inherited

	HTMLElement
}

// newFIGURE
//
func newFIGURE(base interface{}) FIGURE {

	if obj, ok := base.(HTMLElement); ok {

		return &objFIGURE{obj}

	} else {

		return &objFIGURE{newHTMLElement(base)}
	}
}

// UniqueFIGUREInterface
// make sure FIGURE is a unique interface type
func (o *objFIGURE) UniqueFIGUREInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/footer
//
type FOOTER interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueFOOTERInterface()
}

// objFOOTER
//
type objFOOTER struct {

	// inherited

	HTMLElement
}

// newFOOTER
//
func newFOOTER(base interface{}) FOOTER {

	if obj, ok := base.(HTMLElement); ok {

		return &objFOOTER{obj}

	} else {

		return &objFOOTER{newHTMLElement(base)}
	}
}

// UniqueFOOTERInterface
// make sure FOOTER is a unique interface type
func (o *objFOOTER) UniqueFOOTERInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form
//
type FORM interface {

	// inherited

	HTMLFormElement

	// make unique interface

	UniqueFORMInterface()
}

// objFORM
//
type objFORM struct {

	// inherited

	HTMLFormElement
}

// newFORM
//
func newFORM(base interface{}) FORM {

	if obj, ok := base.(HTMLFormElement); ok {

		return &objFORM{obj}

	} else {

		return &objFORM{newHTMLFormElement(base)}
	}
}

// UniqueFORMInterface
// make sure FORM is a unique interface type
func (o *objFORM) UniqueFORMInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
//
type H1 interface {

	// inherited

	HTMLHeadingElement

	// make unique interface

	UniqueH1Interface()
}

// objH1
//
type objH1 struct {

	// inherited

	HTMLHeadingElement
}

// newH1
//
func newH1(base interface{}) H1 {

	if obj, ok := base.(HTMLHeadingElement); ok {

		return &objH1{obj}

	} else {

		return &objH1{newHTMLHeadingElement(base)}
	}
}

// UniqueH1Interface
// make sure H1 is a unique interface type
func (o *objH1) UniqueH1Interface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
//
type H2 interface {

	// inherited

	HTMLHeadingElement

	// make unique interface

	UniqueH2Interface()
}

// objH2
//
type objH2 struct {

	// inherited

	HTMLHeadingElement
}

// newH2
//
func newH2(base interface{}) H2 {

	if obj, ok := base.(HTMLHeadingElement); ok {

		return &objH2{obj}

	} else {

		return &objH2{newHTMLHeadingElement(base)}
	}
}

// UniqueH2Interface
// make sure H2 is a unique interface type
func (o *objH2) UniqueH2Interface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
//
type H3 interface {

	// inherited

	HTMLHeadingElement

	// make unique interface

	UniqueH3Interface()
}

// objH3
//
type objH3 struct {

	// inherited

	HTMLHeadingElement
}

// newH3
//
func newH3(base interface{}) H3 {

	if obj, ok := base.(HTMLHeadingElement); ok {

		return &objH3{obj}

	} else {

		return &objH3{newHTMLHeadingElement(base)}
	}
}

// UniqueH3Interface
// make sure H3 is a unique interface type
func (o *objH3) UniqueH3Interface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
//
type H4 interface {

	// inherited

	HTMLHeadingElement

	// make unique interface

	UniqueH4Interface()
}

// objH4
//
type objH4 struct {

	// inherited

	HTMLHeadingElement
}

// newH4
//
func newH4(base interface{}) H4 {

	if obj, ok := base.(HTMLHeadingElement); ok {

		return &objH4{obj}

	} else {

		return &objH4{newHTMLHeadingElement(base)}
	}
}

// UniqueH4Interface
// make sure H4 is a unique interface type
func (o *objH4) UniqueH4Interface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
//
type H5 interface {

	// inherited

	HTMLHeadingElement

	// make unique interface

	UniqueH5Interface()
}

// objH5
//
type objH5 struct {

	// inherited

	HTMLHeadingElement
}

// newH5
//
func newH5(base interface{}) H5 {

	if obj, ok := base.(HTMLHeadingElement); ok {

		return &objH5{obj}

	} else {

		return &objH5{newHTMLHeadingElement(base)}
	}
}

// UniqueH5Interface
// make sure H5 is a unique interface type
func (o *objH5) UniqueH5Interface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
//
type H6 interface {

	// inherited

	HTMLHeadingElement

	// make unique interface

	UniqueH6Interface()
}

// objH6
//
type objH6 struct {

	// inherited

	HTMLHeadingElement
}

// newH6
//
func newH6(base interface{}) H6 {

	if obj, ok := base.(HTMLHeadingElement); ok {

		return &objH6{obj}

	} else {

		return &objH6{newHTMLHeadingElement(base)}
	}
}

// UniqueH6Interface
// make sure H6 is a unique interface type
func (o *objH6) UniqueH6Interface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/head
//
type HEAD interface {

	// inherited

	HTMLHeadElement

	// make unique interface

	UniqueHEADInterface()
}

// objHEAD
//
type objHEAD struct {

	// inherited

	HTMLHeadElement
}

// newHEAD
//
func newHEAD(base interface{}) HEAD {

	if obj, ok := base.(HTMLHeadElement); ok {

		return &objHEAD{obj}

	} else {

		return &objHEAD{newHTMLHeadElement(base)}
	}
}

// UniqueHEADInterface
// make sure HEAD is a unique interface type
func (o *objHEAD) UniqueHEADInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/header
//
type HEADER interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHEADERInterface()
}

// objHEADER
//
type objHEADER struct {

	// inherited

	HTMLElement
}

// newHEADER
//
func newHEADER(base interface{}) HEADER {

	if obj, ok := base.(HTMLElement); ok {

		return &objHEADER{obj}

	} else {

		return &objHEADER{newHTMLElement(base)}
	}
}

// UniqueHEADERInterface
// make sure HEADER is a unique interface type
func (o *objHEADER) UniqueHEADERInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/hgroup
//
type HGROUP interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHGROUPInterface()
}

// objHGROUP
//
type objHGROUP struct {

	// inherited

	HTMLElement
}

// newHGROUP
//
func newHGROUP(base interface{}) HGROUP {

	if obj, ok := base.(HTMLElement); ok {

		return &objHGROUP{obj}

	} else {

		return &objHGROUP{newHTMLElement(base)}
	}
}

// UniqueHGROUPInterface
// make sure HGROUP is a unique interface type
func (o *objHGROUP) UniqueHGROUPInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/hr
//
type HR interface {

	// inherited

	HTMLHRElement

	// make unique interface

	UniqueHRInterface()
}

// objHR
//
type objHR struct {

	// inherited

	HTMLHRElement
}

// newHR
//
func newHR(base interface{}) HR {

	if obj, ok := base.(HTMLHRElement); ok {

		return &objHR{obj}

	} else {

		return &objHR{newHTMLHRElement(base)}
	}
}

// UniqueHRInterface
// make sure HR is a unique interface type
func (o *objHR) UniqueHRInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/html
//
type HTML interface {

	// inherited

	HTMLHtmlElement

	// make unique interface

	UniqueHTMLInterface()
}

// objHTML
//
type objHTML struct {

	// inherited

	HTMLHtmlElement
}

// newHTML
//
func newHTML(base interface{}) HTML {

	if obj, ok := base.(HTMLHtmlElement); ok {

		return &objHTML{obj}

	} else {

		return &objHTML{newHTMLHtmlElement(base)}
	}
}

// UniqueHTMLInterface
// make sure HTML is a unique interface type
func (o *objHTML) UniqueHTMLInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/i
//
type I interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueIInterface()
}

// objI
//
type objI struct {

	// inherited

	HTMLElement
}

// newI
//
func newI(base interface{}) I {

	if obj, ok := base.(HTMLElement); ok {

		return &objI{obj}

	} else {

		return &objI{newHTMLElement(base)}
	}
}

// UniqueIInterface
// make sure I is a unique interface type
func (o *objI) UniqueIInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe
//
type IFRAME interface {

	// inherited

	HTMLIFrameElement

	// make unique interface

	UniqueIFRAMEInterface()
}

// objIFRAME
//
type objIFRAME struct {

	// inherited

	HTMLIFrameElement
}

// newIFRAME
//
func newIFRAME(base interface{}) IFRAME {

	if obj, ok := base.(HTMLIFrameElement); ok {

		return &objIFRAME{obj}

	} else {

		return &objIFRAME{newHTMLIFrameElement(base)}
	}
}

// UniqueIFRAMEInterface
// make sure IFRAME is a unique interface type
func (o *objIFRAME) UniqueIFRAMEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img
//
type IMG interface {

	// inherited

	HTMLImageElement

	// make unique interface

	UniqueIMGInterface()
}

// objIMG
//
type objIMG struct {

	// inherited

	HTMLImageElement
}

// newIMG
//
func newIMG(base interface{}) IMG {

	if obj, ok := base.(HTMLImageElement); ok {

		return &objIMG{obj}

	} else {

		return &objIMG{newHTMLImageElement(base)}
	}
}

// UniqueIMGInterface
// make sure IMG is a unique interface type
func (o *objIMG) UniqueIMGInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input
//
type INPUT interface {

	// inherited

	HTMLInputElement

	// make unique interface

	UniqueINPUTInterface()
}

// objINPUT
//
type objINPUT struct {

	// inherited

	HTMLInputElement
}

// newINPUT
//
func newINPUT(base interface{}) INPUT {

	if obj, ok := base.(HTMLInputElement); ok {

		return &objINPUT{obj}

	} else {

		return &objINPUT{newHTMLInputElement(base)}
	}
}

// UniqueINPUTInterface
// make sure INPUT is a unique interface type
func (o *objINPUT) UniqueINPUTInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ins
//
type INS interface {

	// inherited

	HTMLModElement

	// make unique interface

	UniqueINSInterface()
}

// objINS
//
type objINS struct {

	// inherited

	HTMLModElement
}

// newINS
//
func newINS(base interface{}) INS {

	if obj, ok := base.(HTMLModElement); ok {

		return &objINS{obj}

	} else {

		return &objINS{newHTMLModElement(base)}
	}
}

// UniqueINSInterface
// make sure INS is a unique interface type
func (o *objINS) UniqueINSInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/kbd
//
type KBD interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueKBDInterface()
}

// objKBD
//
type objKBD struct {

	// inherited

	HTMLElement
}

// newKBD
//
func newKBD(base interface{}) KBD {

	if obj, ok := base.(HTMLElement); ok {

		return &objKBD{obj}

	} else {

		return &objKBD{newHTMLElement(base)}
	}
}

// UniqueKBDInterface
// make sure KBD is a unique interface type
func (o *objKBD) UniqueKBDInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/label
//
type LABEL interface {

	// inherited

	HTMLLabelElement

	// make unique interface

	UniqueLABELInterface()
}

// objLABEL
//
type objLABEL struct {

	// inherited

	HTMLLabelElement
}

// newLABEL
//
func newLABEL(base interface{}) LABEL {

	if obj, ok := base.(HTMLLabelElement); ok {

		return &objLABEL{obj}

	} else {

		return &objLABEL{newHTMLLabelElement(base)}
	}
}

// UniqueLABELInterface
// make sure LABEL is a unique interface type
func (o *objLABEL) UniqueLABELInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/legend
//
type LEGEND interface {

	// inherited

	HTMLLegendElement

	// make unique interface

	UniqueLEGENDInterface()
}

// objLEGEND
//
type objLEGEND struct {

	// inherited

	HTMLLegendElement
}

// newLEGEND
//
func newLEGEND(base interface{}) LEGEND {

	if obj, ok := base.(HTMLLegendElement); ok {

		return &objLEGEND{obj}

	} else {

		return &objLEGEND{newHTMLLegendElement(base)}
	}
}

// UniqueLEGENDInterface
// make sure LEGEND is a unique interface type
func (o *objLEGEND) UniqueLEGENDInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/li
//
type LI interface {

	// inherited

	HTMLLIElement

	// make unique interface

	UniqueLIInterface()
}

// objLI
//
type objLI struct {

	// inherited

	HTMLLIElement
}

// newLI
//
func newLI(base interface{}) LI {

	if obj, ok := base.(HTMLLIElement); ok {

		return &objLI{obj}

	} else {

		return &objLI{newHTMLLIElement(base)}
	}
}

// UniqueLIInterface
// make sure LI is a unique interface type
func (o *objLI) UniqueLIInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link
//
type LINK interface {

	// inherited

	HTMLLinkElement

	// make unique interface

	UniqueLINKInterface()
}

// objLINK
//
type objLINK struct {

	// inherited

	HTMLLinkElement
}

// newLINK
//
func newLINK(base interface{}) LINK {

	if obj, ok := base.(HTMLLinkElement); ok {

		return &objLINK{obj}

	} else {

		return &objLINK{newHTMLLinkElement(base)}
	}
}

// UniqueLINKInterface
// make sure LINK is a unique interface type
func (o *objLINK) UniqueLINKInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/main
//
type MAIN interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueMAINInterface()
}

// objMAIN
//
type objMAIN struct {

	// inherited

	HTMLElement
}

// newMAIN
//
func newMAIN(base interface{}) MAIN {

	if obj, ok := base.(HTMLElement); ok {

		return &objMAIN{obj}

	} else {

		return &objMAIN{newHTMLElement(base)}
	}
}

// UniqueMAINInterface
// make sure MAIN is a unique interface type
func (o *objMAIN) UniqueMAINInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/map
//
type MAP interface {

	// inherited

	HTMLMapElement

	// make unique interface

	UniqueMAPInterface()
}

// objMAP
//
type objMAP struct {

	// inherited

	HTMLMapElement
}

// newMAP
//
func newMAP(base interface{}) MAP {

	if obj, ok := base.(HTMLMapElement); ok {

		return &objMAP{obj}

	} else {

		return &objMAP{newHTMLMapElement(base)}
	}
}

// UniqueMAPInterface
// make sure MAP is a unique interface type
func (o *objMAP) UniqueMAPInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/mark
//
type MARK interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueMARKInterface()
}

// objMARK
//
type objMARK struct {

	// inherited

	HTMLElement
}

// newMARK
//
func newMARK(base interface{}) MARK {

	if obj, ok := base.(HTMLElement); ok {

		return &objMARK{obj}

	} else {

		return &objMARK{newHTMLElement(base)}
	}
}

// UniqueMARKInterface
// make sure MARK is a unique interface type
func (o *objMARK) UniqueMARKInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/menu
//
type MENU interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueMENUInterface()
}

// objMENU
//
type objMENU struct {

	// inherited

	HTMLElement
}

// newMENU
//
func newMENU(base interface{}) MENU {

	if obj, ok := base.(HTMLElement); ok {

		return &objMENU{obj}

	} else {

		return &objMENU{newHTMLElement(base)}
	}
}

// UniqueMENUInterface
// make sure MENU is a unique interface type
func (o *objMENU) UniqueMENUInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta
//
type META interface {

	// inherited

	HTMLMetaElement

	// make unique interface

	UniqueMETAInterface()
}

// objMETA
//
type objMETA struct {

	// inherited

	HTMLMetaElement
}

// newMETA
//
func newMETA(base interface{}) META {

	if obj, ok := base.(HTMLMetaElement); ok {

		return &objMETA{obj}

	} else {

		return &objMETA{newHTMLMetaElement(base)}
	}
}

// UniqueMETAInterface
// make sure META is a unique interface type
func (o *objMETA) UniqueMETAInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meter
//
type METER interface {

	// inherited

	HTMLMeterElement

	// make unique interface

	UniqueMETERInterface()
}

// objMETER
//
type objMETER struct {

	// inherited

	HTMLMeterElement
}

// newMETER
//
func newMETER(base interface{}) METER {

	if obj, ok := base.(HTMLMeterElement); ok {

		return &objMETER{obj}

	} else {

		return &objMETER{newHTMLMeterElement(base)}
	}
}

// UniqueMETERInterface
// make sure METER is a unique interface type
func (o *objMETER) UniqueMETERInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/nav
//
type NAV interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueNAVInterface()
}

// objNAV
//
type objNAV struct {

	// inherited

	HTMLElement
}

// newNAV
//
func newNAV(base interface{}) NAV {

	if obj, ok := base.(HTMLElement); ok {

		return &objNAV{obj}

	} else {

		return &objNAV{newHTMLElement(base)}
	}
}

// UniqueNAVInterface
// make sure NAV is a unique interface type
func (o *objNAV) UniqueNAVInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/noscript
//
type NOSCRIPT interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueNOSCRIPTInterface()
}

// objNOSCRIPT
//
type objNOSCRIPT struct {

	// inherited

	HTMLElement
}

// newNOSCRIPT
//
func newNOSCRIPT(base interface{}) NOSCRIPT {

	if obj, ok := base.(HTMLElement); ok {

		return &objNOSCRIPT{obj}

	} else {

		return &objNOSCRIPT{newHTMLElement(base)}
	}
}

// UniqueNOSCRIPTInterface
// make sure NOSCRIPT is a unique interface type
func (o *objNOSCRIPT) UniqueNOSCRIPTInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/object
//
type OBJECT interface {

	// inherited

	HTMLObjectElement

	// make unique interface

	UniqueOBJECTInterface()
}

// objOBJECT
//
type objOBJECT struct {

	// inherited

	HTMLObjectElement
}

// newOBJECT
//
func newOBJECT(base interface{}) OBJECT {

	if obj, ok := base.(HTMLObjectElement); ok {

		return &objOBJECT{obj}

	} else {

		return &objOBJECT{newHTMLObjectElement(base)}
	}
}

// UniqueOBJECTInterface
// make sure OBJECT is a unique interface type
func (o *objOBJECT) UniqueOBJECTInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ol
//
type OL interface {

	// inherited

	HTMLOListElement

	// make unique interface

	UniqueOLInterface()
}

// objOL
//
type objOL struct {

	// inherited

	HTMLOListElement
}

// newOL
//
func newOL(base interface{}) OL {

	if obj, ok := base.(HTMLOListElement); ok {

		return &objOL{obj}

	} else {

		return &objOL{newHTMLOListElement(base)}
	}
}

// UniqueOLInterface
// make sure OL is a unique interface type
func (o *objOL) UniqueOLInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/optgroup
//
type OPTGROUP interface {

	// inherited

	HTMLOptGroupElement

	// make unique interface

	UniqueOPTGROUPInterface()
}

// objOPTGROUP
//
type objOPTGROUP struct {

	// inherited

	HTMLOptGroupElement
}

// newOPTGROUP
//
func newOPTGROUP(base interface{}) OPTGROUP {

	if obj, ok := base.(HTMLOptGroupElement); ok {

		return &objOPTGROUP{obj}

	} else {

		return &objOPTGROUP{newHTMLOptGroupElement(base)}
	}
}

// UniqueOPTGROUPInterface
// make sure OPTGROUP is a unique interface type
func (o *objOPTGROUP) UniqueOPTGROUPInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/option
//
type OPTION interface {

	// inherited

	HTMLOptionElement

	// make unique interface

	UniqueOPTIONInterface()
}

// objOPTION
//
type objOPTION struct {

	// inherited

	HTMLOptionElement
}

// newOPTION
//
func newOPTION(base interface{}) OPTION {

	if obj, ok := base.(HTMLOptionElement); ok {

		return &objOPTION{obj}

	} else {

		return &objOPTION{newHTMLOptionElement(base)}
	}
}

// UniqueOPTIONInterface
// make sure OPTION is a unique interface type
func (o *objOPTION) UniqueOPTIONInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/output
//
type OUTPUT interface {

	// inherited

	HTMLOutputElement

	// make unique interface

	UniqueOUTPUTInterface()
}

// objOUTPUT
//
type objOUTPUT struct {

	// inherited

	HTMLOutputElement
}

// newOUTPUT
//
func newOUTPUT(base interface{}) OUTPUT {

	if obj, ok := base.(HTMLOutputElement); ok {

		return &objOUTPUT{obj}

	} else {

		return &objOUTPUT{newHTMLOutputElement(base)}
	}
}

// UniqueOUTPUTInterface
// make sure OUTPUT is a unique interface type
func (o *objOUTPUT) UniqueOUTPUTInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/p
//
type P interface {

	// inherited

	HTMLParagraphElement

	// make unique interface

	UniquePInterface()
}

// objP
//
type objP struct {

	// inherited

	HTMLParagraphElement
}

// newP
//
func newP(base interface{}) P {

	if obj, ok := base.(HTMLParagraphElement); ok {

		return &objP{obj}

	} else {

		return &objP{newHTMLParagraphElement(base)}
	}
}

// UniquePInterface
// make sure P is a unique interface type
func (o *objP) UniquePInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/param
//
type PARAM interface {

	// inherited

	HTMLParamElement

	// make unique interface

	UniquePARAMInterface()
}

// objPARAM
//
type objPARAM struct {

	// inherited

	HTMLParamElement
}

// newPARAM
//
func newPARAM(base interface{}) PARAM {

	if obj, ok := base.(HTMLParamElement); ok {

		return &objPARAM{obj}

	} else {

		return &objPARAM{newHTMLParamElement(base)}
	}
}

// UniquePARAMInterface
// make sure PARAM is a unique interface type
func (o *objPARAM) UniquePARAMInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture
//
type PICTURE interface {

	// inherited

	HTMLPictureElement

	// make unique interface

	UniquePICTUREInterface()
}

// objPICTURE
//
type objPICTURE struct {

	// inherited

	HTMLPictureElement
}

// newPICTURE
//
func newPICTURE(base interface{}) PICTURE {

	if obj, ok := base.(HTMLPictureElement); ok {

		return &objPICTURE{obj}

	} else {

		return &objPICTURE{newHTMLPictureElement(base)}
	}
}

// UniquePICTUREInterface
// make sure PICTURE is a unique interface type
func (o *objPICTURE) UniquePICTUREInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/pre
//
type PRE interface {

	// inherited

	HTMLPreElement

	// make unique interface

	UniquePREInterface()
}

// objPRE
//
type objPRE struct {

	// inherited

	HTMLPreElement
}

// newPRE
//
func newPRE(base interface{}) PRE {

	if obj, ok := base.(HTMLPreElement); ok {

		return &objPRE{obj}

	} else {

		return &objPRE{newHTMLPreElement(base)}
	}
}

// UniquePREInterface
// make sure PRE is a unique interface type
func (o *objPRE) UniquePREInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/progress
//
type PROGRESS interface {

	// inherited

	HTMLProgressElement

	// make unique interface

	UniquePROGRESSInterface()
}

// objPROGRESS
//
type objPROGRESS struct {

	// inherited

	HTMLProgressElement
}

// newPROGRESS
//
func newPROGRESS(base interface{}) PROGRESS {

	if obj, ok := base.(HTMLProgressElement); ok {

		return &objPROGRESS{obj}

	} else {

		return &objPROGRESS{newHTMLProgressElement(base)}
	}
}

// UniquePROGRESSInterface
// make sure PROGRESS is a unique interface type
func (o *objPROGRESS) UniquePROGRESSInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/q
//
type Q interface {

	// inherited

	HTMLQuoteElement

	// make unique interface

	UniqueQInterface()
}

// objQ
//
type objQ struct {

	// inherited

	HTMLQuoteElement
}

// newQ
//
func newQ(base interface{}) Q {

	if obj, ok := base.(HTMLQuoteElement); ok {

		return &objQ{obj}

	} else {

		return &objQ{newHTMLQuoteElement(base)}
	}
}

// UniqueQInterface
// make sure Q is a unique interface type
func (o *objQ) UniqueQInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/rb
//
type RB interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueRBInterface()
}

// objRB
//
type objRB struct {

	// inherited

	HTMLElement
}

// newRB
//
func newRB(base interface{}) RB {

	if obj, ok := base.(HTMLElement); ok {

		return &objRB{obj}

	} else {

		return &objRB{newHTMLElement(base)}
	}
}

// UniqueRBInterface
// make sure RB is a unique interface type
func (o *objRB) UniqueRBInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/rp
//
type RP interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueRPInterface()
}

// objRP
//
type objRP struct {

	// inherited

	HTMLElement
}

// newRP
//
func newRP(base interface{}) RP {

	if obj, ok := base.(HTMLElement); ok {

		return &objRP{obj}

	} else {

		return &objRP{newHTMLElement(base)}
	}
}

// UniqueRPInterface
// make sure RP is a unique interface type
func (o *objRP) UniqueRPInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/rt
//
type RT interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueRTInterface()
}

// objRT
//
type objRT struct {

	// inherited

	HTMLElement
}

// newRT
//
func newRT(base interface{}) RT {

	if obj, ok := base.(HTMLElement); ok {

		return &objRT{obj}

	} else {

		return &objRT{newHTMLElement(base)}
	}
}

// UniqueRTInterface
// make sure RT is a unique interface type
func (o *objRT) UniqueRTInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/rtc
//
type RTC interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueRTCInterface()
}

// objRTC
//
type objRTC struct {

	// inherited

	HTMLElement
}

// newRTC
//
func newRTC(base interface{}) RTC {

	if obj, ok := base.(HTMLElement); ok {

		return &objRTC{obj}

	} else {

		return &objRTC{newHTMLElement(base)}
	}
}

// UniqueRTCInterface
// make sure RTC is a unique interface type
func (o *objRTC) UniqueRTCInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ruby
//
type RUBY interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueRUBYInterface()
}

// objRUBY
//
type objRUBY struct {

	// inherited

	HTMLElement
}

// newRUBY
//
func newRUBY(base interface{}) RUBY {

	if obj, ok := base.(HTMLElement); ok {

		return &objRUBY{obj}

	} else {

		return &objRUBY{newHTMLElement(base)}
	}
}

// UniqueRUBYInterface
// make sure RUBY is a unique interface type
func (o *objRUBY) UniqueRUBYInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/s
//
type S interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueSInterface()
}

// objS
//
type objS struct {

	// inherited

	HTMLElement
}

// newS
//
func newS(base interface{}) S {

	if obj, ok := base.(HTMLElement); ok {

		return &objS{obj}

	} else {

		return &objS{newHTMLElement(base)}
	}
}

// UniqueSInterface
// make sure S is a unique interface type
func (o *objS) UniqueSInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/samp
//
type SAMP interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueSAMPInterface()
}

// objSAMP
//
type objSAMP struct {

	// inherited

	HTMLElement
}

// newSAMP
//
func newSAMP(base interface{}) SAMP {

	if obj, ok := base.(HTMLElement); ok {

		return &objSAMP{obj}

	} else {

		return &objSAMP{newHTMLElement(base)}
	}
}

// UniqueSAMPInterface
// make sure SAMP is a unique interface type
func (o *objSAMP) UniqueSAMPInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script
//
type SCRIPT interface {

	// inherited

	HTMLScriptElement

	// make unique interface

	UniqueSCRIPTInterface()
}

// objSCRIPT
//
type objSCRIPT struct {

	// inherited

	HTMLScriptElement
}

// newSCRIPT
//
func newSCRIPT(base interface{}) SCRIPT {

	if obj, ok := base.(HTMLScriptElement); ok {

		return &objSCRIPT{obj}

	} else {

		return &objSCRIPT{newHTMLScriptElement(base)}
	}
}

// UniqueSCRIPTInterface
// make sure SCRIPT is a unique interface type
func (o *objSCRIPT) UniqueSCRIPTInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/section
//
type SECTION interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueSECTIONInterface()
}

// objSECTION
//
type objSECTION struct {

	// inherited

	HTMLElement
}

// newSECTION
//
func newSECTION(base interface{}) SECTION {

	if obj, ok := base.(HTMLElement); ok {

		return &objSECTION{obj}

	} else {

		return &objSECTION{newHTMLElement(base)}
	}
}

// UniqueSECTIONInterface
// make sure SECTION is a unique interface type
func (o *objSECTION) UniqueSECTIONInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/select
//
type SELECT interface {

	// inherited

	HTMLSelectElement

	// make unique interface

	UniqueSELECTInterface()
}

// objSELECT
//
type objSELECT struct {

	// inherited

	HTMLSelectElement
}

// newSELECT
//
func newSELECT(base interface{}) SELECT {

	if obj, ok := base.(HTMLSelectElement); ok {

		return &objSELECT{obj}

	} else {

		return &objSELECT{newHTMLSelectElement(base)}
	}
}

// UniqueSELECTInterface
// make sure SELECT is a unique interface type
func (o *objSELECT) UniqueSELECTInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/slot
//
type SLOT interface {

	// inherited

	HTMLSlotElement

	// make unique interface

	UniqueSLOTInterface()
}

// objSLOT
//
type objSLOT struct {

	// inherited

	HTMLSlotElement
}

// newSLOT
//
func newSLOT(base interface{}) SLOT {

	if obj, ok := base.(HTMLSlotElement); ok {

		return &objSLOT{obj}

	} else {

		return &objSLOT{newHTMLSlotElement(base)}
	}
}

// UniqueSLOTInterface
// make sure SLOT is a unique interface type
func (o *objSLOT) UniqueSLOTInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/small
//
type SMALL interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueSMALLInterface()
}

// objSMALL
//
type objSMALL struct {

	// inherited

	HTMLElement
}

// newSMALL
//
func newSMALL(base interface{}) SMALL {

	if obj, ok := base.(HTMLElement); ok {

		return &objSMALL{obj}

	} else {

		return &objSMALL{newHTMLElement(base)}
	}
}

// UniqueSMALLInterface
// make sure SMALL is a unique interface type
func (o *objSMALL) UniqueSMALLInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/source
//
type SOURCE interface {

	// inherited

	HTMLSourceElement

	// make unique interface

	UniqueSOURCEInterface()
}

// objSOURCE
//
type objSOURCE struct {

	// inherited

	HTMLSourceElement
}

// newSOURCE
//
func newSOURCE(base interface{}) SOURCE {

	if obj, ok := base.(HTMLSourceElement); ok {

		return &objSOURCE{obj}

	} else {

		return &objSOURCE{newHTMLSourceElement(base)}
	}
}

// UniqueSOURCEInterface
// make sure SOURCE is a unique interface type
func (o *objSOURCE) UniqueSOURCEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/span
//
type SPAN interface {

	// inherited

	HTMLSpanElement

	// make unique interface

	UniqueSPANInterface()
}

// objSPAN
//
type objSPAN struct {

	// inherited

	HTMLSpanElement
}

// newSPAN
//
func newSPAN(base interface{}) SPAN {

	if obj, ok := base.(HTMLSpanElement); ok {

		return &objSPAN{obj}

	} else {

		return &objSPAN{newHTMLSpanElement(base)}
	}
}

// UniqueSPANInterface
// make sure SPAN is a unique interface type
func (o *objSPAN) UniqueSPANInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/strong
//
type STRONG interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueSTRONGInterface()
}

// objSTRONG
//
type objSTRONG struct {

	// inherited

	HTMLElement
}

// newSTRONG
//
func newSTRONG(base interface{}) STRONG {

	if obj, ok := base.(HTMLElement); ok {

		return &objSTRONG{obj}

	} else {

		return &objSTRONG{newHTMLElement(base)}
	}
}

// UniqueSTRONGInterface
// make sure STRONG is a unique interface type
func (o *objSTRONG) UniqueSTRONGInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/style
//
type STYLE interface {

	// inherited

	HTMLStyleElement

	// make unique interface

	UniqueSTYLEInterface()
}

// objSTYLE
//
type objSTYLE struct {

	// inherited

	HTMLStyleElement
}

// newSTYLE
//
func newSTYLE(base interface{}) STYLE {

	if obj, ok := base.(HTMLStyleElement); ok {

		return &objSTYLE{obj}

	} else {

		return &objSTYLE{newHTMLStyleElement(base)}
	}
}

// UniqueSTYLEInterface
// make sure STYLE is a unique interface type
func (o *objSTYLE) UniqueSTYLEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/sub
//
type SUB interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueSUBInterface()
}

// objSUB
//
type objSUB struct {

	// inherited

	HTMLElement
}

// newSUB
//
func newSUB(base interface{}) SUB {

	if obj, ok := base.(HTMLElement); ok {

		return &objSUB{obj}

	} else {

		return &objSUB{newHTMLElement(base)}
	}
}

// UniqueSUBInterface
// make sure SUB is a unique interface type
func (o *objSUB) UniqueSUBInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/summary
//
type SUMMARY interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueSUMMARYInterface()
}

// objSUMMARY
//
type objSUMMARY struct {

	// inherited

	HTMLElement
}

// newSUMMARY
//
func newSUMMARY(base interface{}) SUMMARY {

	if obj, ok := base.(HTMLElement); ok {

		return &objSUMMARY{obj}

	} else {

		return &objSUMMARY{newHTMLElement(base)}
	}
}

// UniqueSUMMARYInterface
// make sure SUMMARY is a unique interface type
func (o *objSUMMARY) UniqueSUMMARYInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/sup
//
type SUP interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueSUPInterface()
}

// objSUP
//
type objSUP struct {

	// inherited

	HTMLElement
}

// newSUP
//
func newSUP(base interface{}) SUP {

	if obj, ok := base.(HTMLElement); ok {

		return &objSUP{obj}

	} else {

		return &objSUP{newHTMLElement(base)}
	}
}

// UniqueSUPInterface
// make sure SUP is a unique interface type
func (o *objSUP) UniqueSUPInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table
//
type TABLE interface {

	// inherited

	HTMLTableElement

	// make unique interface

	UniqueTABLEInterface()
}

// objTABLE
//
type objTABLE struct {

	// inherited

	HTMLTableElement
}

// newTABLE
//
func newTABLE(base interface{}) TABLE {

	if obj, ok := base.(HTMLTableElement); ok {

		return &objTABLE{obj}

	} else {

		return &objTABLE{newHTMLTableElement(base)}
	}
}

// UniqueTABLEInterface
// make sure TABLE is a unique interface type
func (o *objTABLE) UniqueTABLEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tbody
//
type TBODY interface {

	// inherited

	HTMLTableSectionElement

	// make unique interface

	UniqueTBODYInterface()
}

// objTBODY
//
type objTBODY struct {

	// inherited

	HTMLTableSectionElement
}

// newTBODY
//
func newTBODY(base interface{}) TBODY {

	if obj, ok := base.(HTMLTableSectionElement); ok {

		return &objTBODY{obj}

	} else {

		return &objTBODY{newHTMLTableSectionElement(base)}
	}
}

// UniqueTBODYInterface
// make sure TBODY is a unique interface type
func (o *objTBODY) UniqueTBODYInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td
//
type TD interface {

	// inherited

	HTMLTableCellElement

	// make unique interface

	UniqueTDInterface()
}

// objTD
//
type objTD struct {

	// inherited

	HTMLTableCellElement
}

// newTD
//
func newTD(base interface{}) TD {

	if obj, ok := base.(HTMLTableCellElement); ok {

		return &objTD{obj}

	} else {

		return &objTD{newHTMLTableCellElement(base)}
	}
}

// UniqueTDInterface
// make sure TD is a unique interface type
func (o *objTD) UniqueTDInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/template
//
type TEMPLATE interface {

	// inherited

	HTMLTemplateElement

	// make unique interface

	UniqueTEMPLATEInterface()
}

// objTEMPLATE
//
type objTEMPLATE struct {

	// inherited

	HTMLTemplateElement
}

// newTEMPLATE
//
func newTEMPLATE(base interface{}) TEMPLATE {

	if obj, ok := base.(HTMLTemplateElement); ok {

		return &objTEMPLATE{obj}

	} else {

		return &objTEMPLATE{newHTMLTemplateElement(base)}
	}
}

// UniqueTEMPLATEInterface
// make sure TEMPLATE is a unique interface type
func (o *objTEMPLATE) UniqueTEMPLATEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/textarea
//
type TEXTAREA interface {

	// inherited

	HTMLTextAreaElement

	// make unique interface

	UniqueTEXTAREAInterface()
}

// objTEXTAREA
//
type objTEXTAREA struct {

	// inherited

	HTMLTextAreaElement
}

// newTEXTAREA
//
func newTEXTAREA(base interface{}) TEXTAREA {

	if obj, ok := base.(HTMLTextAreaElement); ok {

		return &objTEXTAREA{obj}

	} else {

		return &objTEXTAREA{newHTMLTextAreaElement(base)}
	}
}

// UniqueTEXTAREAInterface
// make sure TEXTAREA is a unique interface type
func (o *objTEXTAREA) UniqueTEXTAREAInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tfoot
//
type TFOOT interface {

	// inherited

	HTMLTableSectionElement

	// make unique interface

	UniqueTFOOTInterface()
}

// objTFOOT
//
type objTFOOT struct {

	// inherited

	HTMLTableSectionElement
}

// newTFOOT
//
func newTFOOT(base interface{}) TFOOT {

	if obj, ok := base.(HTMLTableSectionElement); ok {

		return &objTFOOT{obj}

	} else {

		return &objTFOOT{newHTMLTableSectionElement(base)}
	}
}

// UniqueTFOOTInterface
// make sure TFOOT is a unique interface type
func (o *objTFOOT) UniqueTFOOTInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/th
//
type TH interface {

	// inherited

	HTMLTableCellElement

	// make unique interface

	UniqueTHInterface()
}

// objTH
//
type objTH struct {

	// inherited

	HTMLTableCellElement
}

// newTH
//
func newTH(base interface{}) TH {

	if obj, ok := base.(HTMLTableCellElement); ok {

		return &objTH{obj}

	} else {

		return &objTH{newHTMLTableCellElement(base)}
	}
}

// UniqueTHInterface
// make sure TH is a unique interface type
func (o *objTH) UniqueTHInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/thead
//
type THEAD interface {

	// inherited

	HTMLTableSectionElement

	// make unique interface

	UniqueTHEADInterface()
}

// objTHEAD
//
type objTHEAD struct {

	// inherited

	HTMLTableSectionElement
}

// newTHEAD
//
func newTHEAD(base interface{}) THEAD {

	if obj, ok := base.(HTMLTableSectionElement); ok {

		return &objTHEAD{obj}

	} else {

		return &objTHEAD{newHTMLTableSectionElement(base)}
	}
}

// UniqueTHEADInterface
// make sure THEAD is a unique interface type
func (o *objTHEAD) UniqueTHEADInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/time
//
type TIME interface {

	// inherited

	HTMLTimeElement

	// make unique interface

	UniqueTIMEInterface()
}

// objTIME
//
type objTIME struct {

	// inherited

	HTMLTimeElement
}

// newTIME
//
func newTIME(base interface{}) TIME {

	if obj, ok := base.(HTMLTimeElement); ok {

		return &objTIME{obj}

	} else {

		return &objTIME{newHTMLTimeElement(base)}
	}
}

// UniqueTIMEInterface
// make sure TIME is a unique interface type
func (o *objTIME) UniqueTIMEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/title
//
type TITLE interface {

	// inherited

	HTMLTitleElement

	// make unique interface

	UniqueTITLEInterface()
}

// objTITLE
//
type objTITLE struct {

	// inherited

	HTMLTitleElement
}

// newTITLE
//
func newTITLE(base interface{}) TITLE {

	if obj, ok := base.(HTMLTitleElement); ok {

		return &objTITLE{obj}

	} else {

		return &objTITLE{newHTMLTitleElement(base)}
	}
}

// UniqueTITLEInterface
// make sure TITLE is a unique interface type
func (o *objTITLE) UniqueTITLEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tr
//
type TR interface {

	// inherited

	HTMLTableRowElement

	// make unique interface

	UniqueTRInterface()
}

// objTR
//
type objTR struct {

	// inherited

	HTMLTableRowElement
}

// newTR
//
func newTR(base interface{}) TR {

	if obj, ok := base.(HTMLTableRowElement); ok {

		return &objTR{obj}

	} else {

		return &objTR{newHTMLTableRowElement(base)}
	}
}

// UniqueTRInterface
// make sure TR is a unique interface type
func (o *objTR) UniqueTRInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/track
//
type TRACK interface {

	// inherited

	HTMLTrackElement

	// make unique interface

	UniqueTRACKInterface()
}

// objTRACK
//
type objTRACK struct {

	// inherited

	HTMLTrackElement
}

// newTRACK
//
func newTRACK(base interface{}) TRACK {

	if obj, ok := base.(HTMLTrackElement); ok {

		return &objTRACK{obj}

	} else {

		return &objTRACK{newHTMLTrackElement(base)}
	}
}

// UniqueTRACKInterface
// make sure TRACK is a unique interface type
func (o *objTRACK) UniqueTRACKInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base
//
type TYPE interface {

	// inherited

	HTMLBaseElement

	// make unique interface

	UniqueTYPEInterface()
}

// objTYPE
//
type objTYPE struct {

	// inherited

	HTMLBaseElement
}

// newTYPE
//
func newTYPE(base interface{}) TYPE {

	if obj, ok := base.(HTMLBaseElement); ok {

		return &objTYPE{obj}

	} else {

		return &objTYPE{newHTMLBaseElement(base)}
	}
}

// UniqueTYPEInterface
// make sure TYPE is a unique interface type
func (o *objTYPE) UniqueTYPEInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/u
//
type U interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueUInterface()
}

// objU
//
type objU struct {

	// inherited

	HTMLElement
}

// newU
//
func newU(base interface{}) U {

	if obj, ok := base.(HTMLElement); ok {

		return &objU{obj}

	} else {

		return &objU{newHTMLElement(base)}
	}
}

// UniqueUInterface
// make sure U is a unique interface type
func (o *objU) UniqueUInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul
//
type UL interface {

	// inherited

	HTMLUListElement

	// make unique interface

	UniqueULInterface()
}

// objUL
//
type objUL struct {

	// inherited

	HTMLUListElement
}

// newUL
//
func newUL(base interface{}) UL {

	if obj, ok := base.(HTMLUListElement); ok {

		return &objUL{obj}

	} else {

		return &objUL{newHTMLUListElement(base)}
	}
}

// UniqueULInterface
// make sure UL is a unique interface type
func (o *objUL) UniqueULInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/var
//
type VAR interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueVARInterface()
}

// objVAR
//
type objVAR struct {

	// inherited

	HTMLElement
}

// newVAR
//
func newVAR(base interface{}) VAR {

	if obj, ok := base.(HTMLElement); ok {

		return &objVAR{obj}

	} else {

		return &objVAR{newHTMLElement(base)}
	}
}

// UniqueVARInterface
// make sure VAR is a unique interface type
func (o *objVAR) UniqueVARInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video
//
type VIDEO interface {

	// inherited

	HTMLVideoElement

	// make unique interface

	UniqueVIDEOInterface()
}

// objVIDEO
//
type objVIDEO struct {

	// inherited

	HTMLVideoElement
}

// newVIDEO
//
func newVIDEO(base interface{}) VIDEO {

	if obj, ok := base.(HTMLVideoElement); ok {

		return &objVIDEO{obj}

	} else {

		return &objVIDEO{newHTMLVideoElement(base)}
	}
}

// UniqueVIDEOInterface
// make sure VIDEO is a unique interface type
func (o *objVIDEO) UniqueVIDEOInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/wbr
//
type WBR interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueWBRInterface()
}

// objWBR
//
type objWBR struct {

	// inherited

	HTMLElement
}

// newWBR
//
func newWBR(base interface{}) WBR {

	if obj, ok := base.(HTMLElement); ok {

		return &objWBR{obj}

	} else {

		return &objWBR{newHTMLElement(base)}
	}
}

// UniqueWBRInterface
// make sure WBR is a unique interface type
func (o *objWBR) UniqueWBRInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLAnchorElement
//
type HTMLAnchorElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLAnchorElementInterface()
}

// objHTMLAnchorElement
//
type objHTMLAnchorElement struct {

	// inherited

	HTMLElement
}

// newHTMLAnchorElement
//
func newHTMLAnchorElement(base interface{}) HTMLAnchorElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLAnchorElement{obj}

	} else {

		return &objHTMLAnchorElement{newHTMLElement(base)}
	}
}

// UniqueHTMLAnchorElementInterface
// make sure HTMLAnchorElement is a unique interface type
func (o *objHTMLAnchorElement) UniqueHTMLAnchorElementInterface() {}

//
//
type HTMLAppletElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLAppletElementInterface()
}

// objHTMLAppletElement
//
type objHTMLAppletElement struct {

	// inherited

	HTMLElement
}

// newHTMLAppletElement
//
func newHTMLAppletElement(base interface{}) HTMLAppletElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLAppletElement{obj}

	} else {

		return &objHTMLAppletElement{newHTMLElement(base)}
	}
}

// UniqueHTMLAppletElementInterface
// make sure HTMLAppletElement is a unique interface type
func (o *objHTMLAppletElement) UniqueHTMLAppletElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLAreaElement
//
type HTMLAreaElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLAreaElementInterface()
}

// objHTMLAreaElement
//
type objHTMLAreaElement struct {

	// inherited

	HTMLElement
}

// newHTMLAreaElement
//
func newHTMLAreaElement(base interface{}) HTMLAreaElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLAreaElement{obj}

	} else {

		return &objHTMLAreaElement{newHTMLElement(base)}
	}
}

// UniqueHTMLAreaElementInterface
// make sure HTMLAreaElement is a unique interface type
func (o *objHTMLAreaElement) UniqueHTMLAreaElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLAudioElement
//
type HTMLAudioElement interface {

	// inherited

	HTMLMediaElement

	// make unique interface

	UniqueHTMLAudioElementInterface()
}

// objHTMLAudioElement
//
type objHTMLAudioElement struct {

	// inherited

	HTMLMediaElement
}

// newHTMLAudioElement
//
func newHTMLAudioElement(base interface{}) HTMLAudioElement {

	if obj, ok := base.(HTMLMediaElement); ok {

		return &objHTMLAudioElement{obj}

	} else {

		return &objHTMLAudioElement{newHTMLMediaElement(base)}
	}
}

// UniqueHTMLAudioElementInterface
// make sure HTMLAudioElement is a unique interface type
func (o *objHTMLAudioElement) UniqueHTMLAudioElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLBRElement
//
type HTMLBRElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLBRElementInterface()
}

// objHTMLBRElement
//
type objHTMLBRElement struct {

	// inherited

	HTMLElement
}

// newHTMLBRElement
//
func newHTMLBRElement(base interface{}) HTMLBRElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLBRElement{obj}

	} else {

		return &objHTMLBRElement{newHTMLElement(base)}
	}
}

// UniqueHTMLBRElementInterface
// make sure HTMLBRElement is a unique interface type
func (o *objHTMLBRElement) UniqueHTMLBRElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLBaseElement
//
type HTMLBaseElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLBaseElementInterface()
}

// objHTMLBaseElement
//
type objHTMLBaseElement struct {

	// inherited

	HTMLElement
}

// newHTMLBaseElement
//
func newHTMLBaseElement(base interface{}) HTMLBaseElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLBaseElement{obj}

	} else {

		return &objHTMLBaseElement{newHTMLElement(base)}
	}
}

// UniqueHTMLBaseElementInterface
// make sure HTMLBaseElement is a unique interface type
func (o *objHTMLBaseElement) UniqueHTMLBaseElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLBodyElement
//
type HTMLBodyElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLBodyElementInterface()
}

// objHTMLBodyElement
//
type objHTMLBodyElement struct {

	// inherited

	HTMLElement
}

// newHTMLBodyElement
//
func newHTMLBodyElement(base interface{}) HTMLBodyElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLBodyElement{obj}

	} else {

		return &objHTMLBodyElement{newHTMLElement(base)}
	}
}

// UniqueHTMLBodyElementInterface
// make sure HTMLBodyElement is a unique interface type
func (o *objHTMLBodyElement) UniqueHTMLBodyElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLButtonElement
//
type HTMLButtonElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLButtonElementInterface()
}

// objHTMLButtonElement
//
type objHTMLButtonElement struct {

	// inherited

	HTMLElement
}

// newHTMLButtonElement
//
func newHTMLButtonElement(base interface{}) HTMLButtonElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLButtonElement{obj}

	} else {

		return &objHTMLButtonElement{newHTMLElement(base)}
	}
}

// UniqueHTMLButtonElementInterface
// make sure HTMLButtonElement is a unique interface type
func (o *objHTMLButtonElement) UniqueHTMLButtonElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement
//
type HTMLCanvasElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLCanvasElementInterface()
}

// objHTMLCanvasElement
//
type objHTMLCanvasElement struct {

	// inherited

	HTMLElement
}

// newHTMLCanvasElement
//
func newHTMLCanvasElement(base interface{}) HTMLCanvasElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLCanvasElement{obj}

	} else {

		return &objHTMLCanvasElement{newHTMLElement(base)}
	}
}

// UniqueHTMLCanvasElementInterface
// make sure HTMLCanvasElement is a unique interface type
func (o *objHTMLCanvasElement) UniqueHTMLCanvasElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLDListElement
//
type HTMLDListElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLDListElementInterface()
}

// objHTMLDListElement
//
type objHTMLDListElement struct {

	// inherited

	HTMLElement
}

// newHTMLDListElement
//
func newHTMLDListElement(base interface{}) HTMLDListElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLDListElement{obj}

	} else {

		return &objHTMLDListElement{newHTMLElement(base)}
	}
}

// UniqueHTMLDListElementInterface
// make sure HTMLDListElement is a unique interface type
func (o *objHTMLDListElement) UniqueHTMLDListElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLDataElement
//
type HTMLDataElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLDataElementInterface()
}

// objHTMLDataElement
//
type objHTMLDataElement struct {

	// inherited

	HTMLElement
}

// newHTMLDataElement
//
func newHTMLDataElement(base interface{}) HTMLDataElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLDataElement{obj}

	} else {

		return &objHTMLDataElement{newHTMLElement(base)}
	}
}

// UniqueHTMLDataElementInterface
// make sure HTMLDataElement is a unique interface type
func (o *objHTMLDataElement) UniqueHTMLDataElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLDataListElement
//
type HTMLDataListElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLDataListElementInterface()
}

// objHTMLDataListElement
//
type objHTMLDataListElement struct {

	// inherited

	HTMLElement
}

// newHTMLDataListElement
//
func newHTMLDataListElement(base interface{}) HTMLDataListElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLDataListElement{obj}

	} else {

		return &objHTMLDataListElement{newHTMLElement(base)}
	}
}

// UniqueHTMLDataListElementInterface
// make sure HTMLDataListElement is a unique interface type
func (o *objHTMLDataListElement) UniqueHTMLDataListElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLDetailsElement
//
type HTMLDetailsElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLDetailsElementInterface()
}

// objHTMLDetailsElement
//
type objHTMLDetailsElement struct {

	// inherited

	HTMLElement
}

// newHTMLDetailsElement
//
func newHTMLDetailsElement(base interface{}) HTMLDetailsElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLDetailsElement{obj}

	} else {

		return &objHTMLDetailsElement{newHTMLElement(base)}
	}
}

// UniqueHTMLDetailsElementInterface
// make sure HTMLDetailsElement is a unique interface type
func (o *objHTMLDetailsElement) UniqueHTMLDetailsElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLDialogElement
//
type HTMLDialogElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLDialogElementInterface()
}

// objHTMLDialogElement
//
type objHTMLDialogElement struct {

	// inherited

	HTMLElement
}

// newHTMLDialogElement
//
func newHTMLDialogElement(base interface{}) HTMLDialogElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLDialogElement{obj}

	} else {

		return &objHTMLDialogElement{newHTMLElement(base)}
	}
}

// UniqueHTMLDialogElementInterface
// make sure HTMLDialogElement is a unique interface type
func (o *objHTMLDialogElement) UniqueHTMLDialogElementInterface() {}

//
//
type HTMLDirectoryElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLDirectoryElementInterface()
}

// objHTMLDirectoryElement
//
type objHTMLDirectoryElement struct {

	// inherited

	HTMLElement
}

// newHTMLDirectoryElement
//
func newHTMLDirectoryElement(base interface{}) HTMLDirectoryElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLDirectoryElement{obj}

	} else {

		return &objHTMLDirectoryElement{newHTMLElement(base)}
	}
}

// UniqueHTMLDirectoryElementInterface
// make sure HTMLDirectoryElement is a unique interface type
func (o *objHTMLDirectoryElement) UniqueHTMLDirectoryElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLDivElement
//
type HTMLDivElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLDivElementInterface()
}

// objHTMLDivElement
//
type objHTMLDivElement struct {

	// inherited

	HTMLElement
}

// newHTMLDivElement
//
func newHTMLDivElement(base interface{}) HTMLDivElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLDivElement{obj}

	} else {

		return &objHTMLDivElement{newHTMLElement(base)}
	}
}

// UniqueHTMLDivElementInterface
// make sure HTMLDivElement is a unique interface type
func (o *objHTMLDivElement) UniqueHTMLDivElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement
//
type HTMLElement interface {

	// inherited

	Element

	// make unique interface

	UniqueHTMLElementInterface()
}

// objHTMLElement
//
type objHTMLElement struct {

	// inherited

	Element
}

// newHTMLElement
//
func newHTMLElement(base interface{}) HTMLElement {

	if obj, ok := base.(Element); ok {

		return &objHTMLElement{obj}

	} else {

		return &objHTMLElement{newElement(base)}
	}
}

// UniqueHTMLElementInterface
// make sure HTMLElement is a unique interface type
func (o *objHTMLElement) UniqueHTMLElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLEmbedElement
//
type HTMLEmbedElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLEmbedElementInterface()
}

// objHTMLEmbedElement
//
type objHTMLEmbedElement struct {

	// inherited

	HTMLElement
}

// newHTMLEmbedElement
//
func newHTMLEmbedElement(base interface{}) HTMLEmbedElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLEmbedElement{obj}

	} else {

		return &objHTMLEmbedElement{newHTMLElement(base)}
	}
}

// UniqueHTMLEmbedElementInterface
// make sure HTMLEmbedElement is a unique interface type
func (o *objHTMLEmbedElement) UniqueHTMLEmbedElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLFieldSetElement
//
type HTMLFieldSetElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLFieldSetElementInterface()
}

// objHTMLFieldSetElement
//
type objHTMLFieldSetElement struct {

	// inherited

	HTMLElement
}

// newHTMLFieldSetElement
//
func newHTMLFieldSetElement(base interface{}) HTMLFieldSetElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLFieldSetElement{obj}

	} else {

		return &objHTMLFieldSetElement{newHTMLElement(base)}
	}
}

// UniqueHTMLFieldSetElementInterface
// make sure HTMLFieldSetElement is a unique interface type
func (o *objHTMLFieldSetElement) UniqueHTMLFieldSetElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement
//
type HTMLFormElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLFormElementInterface()
}

// objHTMLFormElement
//
type objHTMLFormElement struct {

	// inherited

	HTMLElement
}

// newHTMLFormElement
//
func newHTMLFormElement(base interface{}) HTMLFormElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLFormElement{obj}

	} else {

		return &objHTMLFormElement{newHTMLElement(base)}
	}
}

// UniqueHTMLFormElementInterface
// make sure HTMLFormElement is a unique interface type
func (o *objHTMLFormElement) UniqueHTMLFormElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLHRElement
//
type HTMLHRElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLHRElementInterface()
}

// objHTMLHRElement
//
type objHTMLHRElement struct {

	// inherited

	HTMLElement
}

// newHTMLHRElement
//
func newHTMLHRElement(base interface{}) HTMLHRElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLHRElement{obj}

	} else {

		return &objHTMLHRElement{newHTMLElement(base)}
	}
}

// UniqueHTMLHRElementInterface
// make sure HTMLHRElement is a unique interface type
func (o *objHTMLHRElement) UniqueHTMLHRElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLHeadElement
//
type HTMLHeadElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLHeadElementInterface()
}

// objHTMLHeadElement
//
type objHTMLHeadElement struct {

	// inherited

	HTMLElement
}

// newHTMLHeadElement
//
func newHTMLHeadElement(base interface{}) HTMLHeadElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLHeadElement{obj}

	} else {

		return &objHTMLHeadElement{newHTMLElement(base)}
	}
}

// UniqueHTMLHeadElementInterface
// make sure HTMLHeadElement is a unique interface type
func (o *objHTMLHeadElement) UniqueHTMLHeadElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLHeadingElement
//
type HTMLHeadingElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLHeadingElementInterface()
}

// objHTMLHeadingElement
//
type objHTMLHeadingElement struct {

	// inherited

	HTMLElement
}

// newHTMLHeadingElement
//
func newHTMLHeadingElement(base interface{}) HTMLHeadingElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLHeadingElement{obj}

	} else {

		return &objHTMLHeadingElement{newHTMLElement(base)}
	}
}

// UniqueHTMLHeadingElementInterface
// make sure HTMLHeadingElement is a unique interface type
func (o *objHTMLHeadingElement) UniqueHTMLHeadingElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLHtmlElement
//
type HTMLHtmlElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLHtmlElementInterface()
}

// objHTMLHtmlElement
//
type objHTMLHtmlElement struct {

	// inherited

	HTMLElement
}

// newHTMLHtmlElement
//
func newHTMLHtmlElement(base interface{}) HTMLHtmlElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLHtmlElement{obj}

	} else {

		return &objHTMLHtmlElement{newHTMLElement(base)}
	}
}

// UniqueHTMLHtmlElementInterface
// make sure HTMLHtmlElement is a unique interface type
func (o *objHTMLHtmlElement) UniqueHTMLHtmlElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLIFrameElement
//
type HTMLIFrameElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLIFrameElementInterface()
}

// objHTMLIFrameElement
//
type objHTMLIFrameElement struct {

	// inherited

	HTMLElement
}

// newHTMLIFrameElement
//
func newHTMLIFrameElement(base interface{}) HTMLIFrameElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLIFrameElement{obj}

	} else {

		return &objHTMLIFrameElement{newHTMLElement(base)}
	}
}

// UniqueHTMLIFrameElementInterface
// make sure HTMLIFrameElement is a unique interface type
func (o *objHTMLIFrameElement) UniqueHTMLIFrameElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement
//
type HTMLImageElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLImageElementInterface()
}

// objHTMLImageElement
//
type objHTMLImageElement struct {

	// inherited

	HTMLElement
}

// newHTMLImageElement
//
func newHTMLImageElement(base interface{}) HTMLImageElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLImageElement{obj}

	} else {

		return &objHTMLImageElement{newHTMLElement(base)}
	}
}

// UniqueHTMLImageElementInterface
// make sure HTMLImageElement is a unique interface type
func (o *objHTMLImageElement) UniqueHTMLImageElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLInputElement
//
type HTMLInputElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLInputElementInterface()
}

// objHTMLInputElement
//
type objHTMLInputElement struct {

	// inherited

	HTMLElement
}

// newHTMLInputElement
//
func newHTMLInputElement(base interface{}) HTMLInputElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLInputElement{obj}

	} else {

		return &objHTMLInputElement{newHTMLElement(base)}
	}
}

// UniqueHTMLInputElementInterface
// make sure HTMLInputElement is a unique interface type
func (o *objHTMLInputElement) UniqueHTMLInputElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLLIElement
//
type HTMLLIElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLLIElementInterface()
}

// objHTMLLIElement
//
type objHTMLLIElement struct {

	// inherited

	HTMLElement
}

// newHTMLLIElement
//
func newHTMLLIElement(base interface{}) HTMLLIElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLLIElement{obj}

	} else {

		return &objHTMLLIElement{newHTMLElement(base)}
	}
}

// UniqueHTMLLIElementInterface
// make sure HTMLLIElement is a unique interface type
func (o *objHTMLLIElement) UniqueHTMLLIElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLLabelElement
//
type HTMLLabelElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLLabelElementInterface()
}

// objHTMLLabelElement
//
type objHTMLLabelElement struct {

	// inherited

	HTMLElement
}

// newHTMLLabelElement
//
func newHTMLLabelElement(base interface{}) HTMLLabelElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLLabelElement{obj}

	} else {

		return &objHTMLLabelElement{newHTMLElement(base)}
	}
}

// UniqueHTMLLabelElementInterface
// make sure HTMLLabelElement is a unique interface type
func (o *objHTMLLabelElement) UniqueHTMLLabelElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLLegendElement
//
type HTMLLegendElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLLegendElementInterface()
}

// objHTMLLegendElement
//
type objHTMLLegendElement struct {

	// inherited

	HTMLElement
}

// newHTMLLegendElement
//
func newHTMLLegendElement(base interface{}) HTMLLegendElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLLegendElement{obj}

	} else {

		return &objHTMLLegendElement{newHTMLElement(base)}
	}
}

// UniqueHTMLLegendElementInterface
// make sure HTMLLegendElement is a unique interface type
func (o *objHTMLLegendElement) UniqueHTMLLegendElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLLinkElement
//
type HTMLLinkElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLLinkElementInterface()
}

// objHTMLLinkElement
//
type objHTMLLinkElement struct {

	// inherited

	HTMLElement
}

// newHTMLLinkElement
//
func newHTMLLinkElement(base interface{}) HTMLLinkElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLLinkElement{obj}

	} else {

		return &objHTMLLinkElement{newHTMLElement(base)}
	}
}

// UniqueHTMLLinkElementInterface
// make sure HTMLLinkElement is a unique interface type
func (o *objHTMLLinkElement) UniqueHTMLLinkElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLMapElement
//
type HTMLMapElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLMapElementInterface()
}

// objHTMLMapElement
//
type objHTMLMapElement struct {

	// inherited

	HTMLElement
}

// newHTMLMapElement
//
func newHTMLMapElement(base interface{}) HTMLMapElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLMapElement{obj}

	} else {

		return &objHTMLMapElement{newHTMLElement(base)}
	}
}

// UniqueHTMLMapElementInterface
// make sure HTMLMapElement is a unique interface type
func (o *objHTMLMapElement) UniqueHTMLMapElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement
//
type HTMLMediaElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLMediaElementInterface()
}

// objHTMLMediaElement
//
type objHTMLMediaElement struct {

	// inherited

	HTMLElement
}

// newHTMLMediaElement
//
func newHTMLMediaElement(base interface{}) HTMLMediaElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLMediaElement{obj}

	} else {

		return &objHTMLMediaElement{newHTMLElement(base)}
	}
}

// UniqueHTMLMediaElementInterface
// make sure HTMLMediaElement is a unique interface type
func (o *objHTMLMediaElement) UniqueHTMLMediaElementInterface() {}

//
//
type HTMLMenuElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLMenuElementInterface()
}

// objHTMLMenuElement
//
type objHTMLMenuElement struct {

	// inherited

	HTMLElement
}

// newHTMLMenuElement
//
func newHTMLMenuElement(base interface{}) HTMLMenuElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLMenuElement{obj}

	} else {

		return &objHTMLMenuElement{newHTMLElement(base)}
	}
}

// UniqueHTMLMenuElementInterface
// make sure HTMLMenuElement is a unique interface type
func (o *objHTMLMenuElement) UniqueHTMLMenuElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLMetaElement
//
type HTMLMetaElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLMetaElementInterface()
}

// objHTMLMetaElement
//
type objHTMLMetaElement struct {

	// inherited

	HTMLElement
}

// newHTMLMetaElement
//
func newHTMLMetaElement(base interface{}) HTMLMetaElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLMetaElement{obj}

	} else {

		return &objHTMLMetaElement{newHTMLElement(base)}
	}
}

// UniqueHTMLMetaElementInterface
// make sure HTMLMetaElement is a unique interface type
func (o *objHTMLMetaElement) UniqueHTMLMetaElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLMeterElement
//
type HTMLMeterElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLMeterElementInterface()
}

// objHTMLMeterElement
//
type objHTMLMeterElement struct {

	// inherited

	HTMLElement
}

// newHTMLMeterElement
//
func newHTMLMeterElement(base interface{}) HTMLMeterElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLMeterElement{obj}

	} else {

		return &objHTMLMeterElement{newHTMLElement(base)}
	}
}

// UniqueHTMLMeterElementInterface
// make sure HTMLMeterElement is a unique interface type
func (o *objHTMLMeterElement) UniqueHTMLMeterElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLModElement
//
type HTMLModElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLModElementInterface()
}

// objHTMLModElement
//
type objHTMLModElement struct {

	// inherited

	HTMLElement
}

// newHTMLModElement
//
func newHTMLModElement(base interface{}) HTMLModElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLModElement{obj}

	} else {

		return &objHTMLModElement{newHTMLElement(base)}
	}
}

// UniqueHTMLModElementInterface
// make sure HTMLModElement is a unique interface type
func (o *objHTMLModElement) UniqueHTMLModElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLOListElement
//
type HTMLOListElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLOListElementInterface()
}

// objHTMLOListElement
//
type objHTMLOListElement struct {

	// inherited

	HTMLElement
}

// newHTMLOListElement
//
func newHTMLOListElement(base interface{}) HTMLOListElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLOListElement{obj}

	} else {

		return &objHTMLOListElement{newHTMLElement(base)}
	}
}

// UniqueHTMLOListElementInterface
// make sure HTMLOListElement is a unique interface type
func (o *objHTMLOListElement) UniqueHTMLOListElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLObjectElement
//
type HTMLObjectElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLObjectElementInterface()
}

// objHTMLObjectElement
//
type objHTMLObjectElement struct {

	// inherited

	HTMLElement
}

// newHTMLObjectElement
//
func newHTMLObjectElement(base interface{}) HTMLObjectElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLObjectElement{obj}

	} else {

		return &objHTMLObjectElement{newHTMLElement(base)}
	}
}

// UniqueHTMLObjectElementInterface
// make sure HTMLObjectElement is a unique interface type
func (o *objHTMLObjectElement) UniqueHTMLObjectElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLOptGroupElement
//
type HTMLOptGroupElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLOptGroupElementInterface()
}

// objHTMLOptGroupElement
//
type objHTMLOptGroupElement struct {

	// inherited

	HTMLElement
}

// newHTMLOptGroupElement
//
func newHTMLOptGroupElement(base interface{}) HTMLOptGroupElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLOptGroupElement{obj}

	} else {

		return &objHTMLOptGroupElement{newHTMLElement(base)}
	}
}

// UniqueHTMLOptGroupElementInterface
// make sure HTMLOptGroupElement is a unique interface type
func (o *objHTMLOptGroupElement) UniqueHTMLOptGroupElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLOptionElement
//
type HTMLOptionElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLOptionElementInterface()
}

// objHTMLOptionElement
//
type objHTMLOptionElement struct {

	// inherited

	HTMLElement
}

// newHTMLOptionElement
//
func newHTMLOptionElement(base interface{}) HTMLOptionElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLOptionElement{obj}

	} else {

		return &objHTMLOptionElement{newHTMLElement(base)}
	}
}

// UniqueHTMLOptionElementInterface
// make sure HTMLOptionElement is a unique interface type
func (o *objHTMLOptionElement) UniqueHTMLOptionElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLOutputElement
//
type HTMLOutputElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLOutputElementInterface()
}

// objHTMLOutputElement
//
type objHTMLOutputElement struct {

	// inherited

	HTMLElement
}

// newHTMLOutputElement
//
func newHTMLOutputElement(base interface{}) HTMLOutputElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLOutputElement{obj}

	} else {

		return &objHTMLOutputElement{newHTMLElement(base)}
	}
}

// UniqueHTMLOutputElementInterface
// make sure HTMLOutputElement is a unique interface type
func (o *objHTMLOutputElement) UniqueHTMLOutputElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLParagraphElement
//
type HTMLParagraphElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLParagraphElementInterface()
}

// objHTMLParagraphElement
//
type objHTMLParagraphElement struct {

	// inherited

	HTMLElement
}

// newHTMLParagraphElement
//
func newHTMLParagraphElement(base interface{}) HTMLParagraphElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLParagraphElement{obj}

	} else {

		return &objHTMLParagraphElement{newHTMLElement(base)}
	}
}

// UniqueHTMLParagraphElementInterface
// make sure HTMLParagraphElement is a unique interface type
func (o *objHTMLParagraphElement) UniqueHTMLParagraphElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLParamElement
//
type HTMLParamElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLParamElementInterface()
}

// objHTMLParamElement
//
type objHTMLParamElement struct {

	// inherited

	HTMLElement
}

// newHTMLParamElement
//
func newHTMLParamElement(base interface{}) HTMLParamElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLParamElement{obj}

	} else {

		return &objHTMLParamElement{newHTMLElement(base)}
	}
}

// UniqueHTMLParamElementInterface
// make sure HTMLParamElement is a unique interface type
func (o *objHTMLParamElement) UniqueHTMLParamElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLPictureElement
//
type HTMLPictureElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLPictureElementInterface()
}

// objHTMLPictureElement
//
type objHTMLPictureElement struct {

	// inherited

	HTMLElement
}

// newHTMLPictureElement
//
func newHTMLPictureElement(base interface{}) HTMLPictureElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLPictureElement{obj}

	} else {

		return &objHTMLPictureElement{newHTMLElement(base)}
	}
}

// UniqueHTMLPictureElementInterface
// make sure HTMLPictureElement is a unique interface type
func (o *objHTMLPictureElement) UniqueHTMLPictureElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLPreElement
//
type HTMLPreElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLPreElementInterface()
}

// objHTMLPreElement
//
type objHTMLPreElement struct {

	// inherited

	HTMLElement
}

// newHTMLPreElement
//
func newHTMLPreElement(base interface{}) HTMLPreElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLPreElement{obj}

	} else {

		return &objHTMLPreElement{newHTMLElement(base)}
	}
}

// UniqueHTMLPreElementInterface
// make sure HTMLPreElement is a unique interface type
func (o *objHTMLPreElement) UniqueHTMLPreElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLProgressElement
//
type HTMLProgressElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLProgressElementInterface()
}

// objHTMLProgressElement
//
type objHTMLProgressElement struct {

	// inherited

	HTMLElement
}

// newHTMLProgressElement
//
func newHTMLProgressElement(base interface{}) HTMLProgressElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLProgressElement{obj}

	} else {

		return &objHTMLProgressElement{newHTMLElement(base)}
	}
}

// UniqueHTMLProgressElementInterface
// make sure HTMLProgressElement is a unique interface type
func (o *objHTMLProgressElement) UniqueHTMLProgressElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLQuoteElement
//
type HTMLQuoteElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLQuoteElementInterface()
}

// objHTMLQuoteElement
//
type objHTMLQuoteElement struct {

	// inherited

	HTMLElement
}

// newHTMLQuoteElement
//
func newHTMLQuoteElement(base interface{}) HTMLQuoteElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLQuoteElement{obj}

	} else {

		return &objHTMLQuoteElement{newHTMLElement(base)}
	}
}

// UniqueHTMLQuoteElementInterface
// make sure HTMLQuoteElement is a unique interface type
func (o *objHTMLQuoteElement) UniqueHTMLQuoteElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLScriptElement
//
type HTMLScriptElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLScriptElementInterface()
}

// objHTMLScriptElement
//
type objHTMLScriptElement struct {

	// inherited

	HTMLElement
}

// newHTMLScriptElement
//
func newHTMLScriptElement(base interface{}) HTMLScriptElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLScriptElement{obj}

	} else {

		return &objHTMLScriptElement{newHTMLElement(base)}
	}
}

// UniqueHTMLScriptElementInterface
// make sure HTMLScriptElement is a unique interface type
func (o *objHTMLScriptElement) UniqueHTMLScriptElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement
//
type HTMLSelectElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLSelectElementInterface()
}

// objHTMLSelectElement
//
type objHTMLSelectElement struct {

	// inherited

	HTMLElement
}

// newHTMLSelectElement
//
func newHTMLSelectElement(base interface{}) HTMLSelectElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLSelectElement{obj}

	} else {

		return &objHTMLSelectElement{newHTMLElement(base)}
	}
}

// UniqueHTMLSelectElementInterface
// make sure HTMLSelectElement is a unique interface type
func (o *objHTMLSelectElement) UniqueHTMLSelectElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLSlotElement
//
type HTMLSlotElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLSlotElementInterface()
}

// objHTMLSlotElement
//
type objHTMLSlotElement struct {

	// inherited

	HTMLElement
}

// newHTMLSlotElement
//
func newHTMLSlotElement(base interface{}) HTMLSlotElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLSlotElement{obj}

	} else {

		return &objHTMLSlotElement{newHTMLElement(base)}
	}
}

// UniqueHTMLSlotElementInterface
// make sure HTMLSlotElement is a unique interface type
func (o *objHTMLSlotElement) UniqueHTMLSlotElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLSourceElement
//
type HTMLSourceElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLSourceElementInterface()
}

// objHTMLSourceElement
//
type objHTMLSourceElement struct {

	// inherited

	HTMLElement
}

// newHTMLSourceElement
//
func newHTMLSourceElement(base interface{}) HTMLSourceElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLSourceElement{obj}

	} else {

		return &objHTMLSourceElement{newHTMLElement(base)}
	}
}

// UniqueHTMLSourceElementInterface
// make sure HTMLSourceElement is a unique interface type
func (o *objHTMLSourceElement) UniqueHTMLSourceElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLSpanElement
//
type HTMLSpanElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLSpanElementInterface()
}

// objHTMLSpanElement
//
type objHTMLSpanElement struct {

	// inherited

	HTMLElement
}

// newHTMLSpanElement
//
func newHTMLSpanElement(base interface{}) HTMLSpanElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLSpanElement{obj}

	} else {

		return &objHTMLSpanElement{newHTMLElement(base)}
	}
}

// UniqueHTMLSpanElementInterface
// make sure HTMLSpanElement is a unique interface type
func (o *objHTMLSpanElement) UniqueHTMLSpanElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLStyleElement
//
type HTMLStyleElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLStyleElementInterface()
}

// objHTMLStyleElement
//
type objHTMLStyleElement struct {

	// inherited

	HTMLElement
}

// newHTMLStyleElement
//
func newHTMLStyleElement(base interface{}) HTMLStyleElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLStyleElement{obj}

	} else {

		return &objHTMLStyleElement{newHTMLElement(base)}
	}
}

// UniqueHTMLStyleElementInterface
// make sure HTMLStyleElement is a unique interface type
func (o *objHTMLStyleElement) UniqueHTMLStyleElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableCaptionElement
//
type HTMLTableCaptionElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLTableCaptionElementInterface()
}

// objHTMLTableCaptionElement
//
type objHTMLTableCaptionElement struct {

	// inherited

	HTMLElement
}

// newHTMLTableCaptionElement
//
func newHTMLTableCaptionElement(base interface{}) HTMLTableCaptionElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLTableCaptionElement{obj}

	} else {

		return &objHTMLTableCaptionElement{newHTMLElement(base)}
	}
}

// UniqueHTMLTableCaptionElementInterface
// make sure HTMLTableCaptionElement is a unique interface type
func (o *objHTMLTableCaptionElement) UniqueHTMLTableCaptionElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableCellElement
//
type HTMLTableCellElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLTableCellElementInterface()
}

// objHTMLTableCellElement
//
type objHTMLTableCellElement struct {

	// inherited

	HTMLElement
}

// newHTMLTableCellElement
//
func newHTMLTableCellElement(base interface{}) HTMLTableCellElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLTableCellElement{obj}

	} else {

		return &objHTMLTableCellElement{newHTMLElement(base)}
	}
}

// UniqueHTMLTableCellElementInterface
// make sure HTMLTableCellElement is a unique interface type
func (o *objHTMLTableCellElement) UniqueHTMLTableCellElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableColElement
//
type HTMLTableColElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLTableColElementInterface()
}

// objHTMLTableColElement
//
type objHTMLTableColElement struct {

	// inherited

	HTMLElement
}

// newHTMLTableColElement
//
func newHTMLTableColElement(base interface{}) HTMLTableColElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLTableColElement{obj}

	} else {

		return &objHTMLTableColElement{newHTMLElement(base)}
	}
}

// UniqueHTMLTableColElementInterface
// make sure HTMLTableColElement is a unique interface type
func (o *objHTMLTableColElement) UniqueHTMLTableColElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableElement
//
type HTMLTableElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLTableElementInterface()
}

// objHTMLTableElement
//
type objHTMLTableElement struct {

	// inherited

	HTMLElement
}

// newHTMLTableElement
//
func newHTMLTableElement(base interface{}) HTMLTableElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLTableElement{obj}

	} else {

		return &objHTMLTableElement{newHTMLElement(base)}
	}
}

// UniqueHTMLTableElementInterface
// make sure HTMLTableElement is a unique interface type
func (o *objHTMLTableElement) UniqueHTMLTableElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableRowElement
//
type HTMLTableRowElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLTableRowElementInterface()
}

// objHTMLTableRowElement
//
type objHTMLTableRowElement struct {

	// inherited

	HTMLElement
}

// newHTMLTableRowElement
//
func newHTMLTableRowElement(base interface{}) HTMLTableRowElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLTableRowElement{obj}

	} else {

		return &objHTMLTableRowElement{newHTMLElement(base)}
	}
}

// UniqueHTMLTableRowElementInterface
// make sure HTMLTableRowElement is a unique interface type
func (o *objHTMLTableRowElement) UniqueHTMLTableRowElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableSectionElement
//
type HTMLTableSectionElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLTableSectionElementInterface()
}

// objHTMLTableSectionElement
//
type objHTMLTableSectionElement struct {

	// inherited

	HTMLElement
}

// newHTMLTableSectionElement
//
func newHTMLTableSectionElement(base interface{}) HTMLTableSectionElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLTableSectionElement{obj}

	} else {

		return &objHTMLTableSectionElement{newHTMLElement(base)}
	}
}

// UniqueHTMLTableSectionElementInterface
// make sure HTMLTableSectionElement is a unique interface type
func (o *objHTMLTableSectionElement) UniqueHTMLTableSectionElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLTemplateElement
//
type HTMLTemplateElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLTemplateElementInterface()
}

// objHTMLTemplateElement
//
type objHTMLTemplateElement struct {

	// inherited

	HTMLElement
}

// newHTMLTemplateElement
//
func newHTMLTemplateElement(base interface{}) HTMLTemplateElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLTemplateElement{obj}

	} else {

		return &objHTMLTemplateElement{newHTMLElement(base)}
	}
}

// UniqueHTMLTemplateElementInterface
// make sure HTMLTemplateElement is a unique interface type
func (o *objHTMLTemplateElement) UniqueHTMLTemplateElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLTextAreaElement
//
type HTMLTextAreaElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLTextAreaElementInterface()
}

// objHTMLTextAreaElement
//
type objHTMLTextAreaElement struct {

	// inherited

	HTMLElement
}

// newHTMLTextAreaElement
//
func newHTMLTextAreaElement(base interface{}) HTMLTextAreaElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLTextAreaElement{obj}

	} else {

		return &objHTMLTextAreaElement{newHTMLElement(base)}
	}
}

// UniqueHTMLTextAreaElementInterface
// make sure HTMLTextAreaElement is a unique interface type
func (o *objHTMLTextAreaElement) UniqueHTMLTextAreaElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLTimeElement
//
type HTMLTimeElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLTimeElementInterface()
}

// objHTMLTimeElement
//
type objHTMLTimeElement struct {

	// inherited

	HTMLElement
}

// newHTMLTimeElement
//
func newHTMLTimeElement(base interface{}) HTMLTimeElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLTimeElement{obj}

	} else {

		return &objHTMLTimeElement{newHTMLElement(base)}
	}
}

// UniqueHTMLTimeElementInterface
// make sure HTMLTimeElement is a unique interface type
func (o *objHTMLTimeElement) UniqueHTMLTimeElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLTitleElement
//
type HTMLTitleElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLTitleElementInterface()
}

// objHTMLTitleElement
//
type objHTMLTitleElement struct {

	// inherited

	HTMLElement
}

// newHTMLTitleElement
//
func newHTMLTitleElement(base interface{}) HTMLTitleElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLTitleElement{obj}

	} else {

		return &objHTMLTitleElement{newHTMLElement(base)}
	}
}

// UniqueHTMLTitleElementInterface
// make sure HTMLTitleElement is a unique interface type
func (o *objHTMLTitleElement) UniqueHTMLTitleElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLTrackElement
//
type HTMLTrackElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLTrackElementInterface()
}

// objHTMLTrackElement
//
type objHTMLTrackElement struct {

	// inherited

	HTMLElement
}

// newHTMLTrackElement
//
func newHTMLTrackElement(base interface{}) HTMLTrackElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLTrackElement{obj}

	} else {

		return &objHTMLTrackElement{newHTMLElement(base)}
	}
}

// UniqueHTMLTrackElementInterface
// make sure HTMLTrackElement is a unique interface type
func (o *objHTMLTrackElement) UniqueHTMLTrackElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLUListElement
//
type HTMLUListElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLUListElementInterface()
}

// objHTMLUListElement
//
type objHTMLUListElement struct {

	// inherited

	HTMLElement
}

// newHTMLUListElement
//
func newHTMLUListElement(base interface{}) HTMLUListElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLUListElement{obj}

	} else {

		return &objHTMLUListElement{newHTMLElement(base)}
	}
}

// UniqueHTMLUListElementInterface
// make sure HTMLUListElement is a unique interface type
func (o *objHTMLUListElement) UniqueHTMLUListElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLUnknownElement
//
type HTMLUnknownElement interface {

	// inherited

	HTMLElement

	// make unique interface

	UniqueHTMLUnknownElementInterface()
}

// objHTMLUnknownElement
//
type objHTMLUnknownElement struct {

	// inherited

	HTMLElement
}

// newHTMLUnknownElement
//
func newHTMLUnknownElement(base interface{}) HTMLUnknownElement {

	if obj, ok := base.(HTMLElement); ok {

		return &objHTMLUnknownElement{obj}

	} else {

		return &objHTMLUnknownElement{newHTMLElement(base)}
	}
}

// UniqueHTMLUnknownElementInterface
// make sure HTMLUnknownElement is a unique interface type
func (o *objHTMLUnknownElement) UniqueHTMLUnknownElementInterface() {}

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLVideoElement
//
type HTMLVideoElement interface {

	// inherited

	HTMLMediaElement

	// make unique interface

	UniqueHTMLVideoElementInterface()
}

// objHTMLVideoElement
//
type objHTMLVideoElement struct {

	// inherited

	HTMLMediaElement
}

// newHTMLVideoElement
//
func newHTMLVideoElement(base interface{}) HTMLVideoElement {

	if obj, ok := base.(HTMLMediaElement); ok {

		return &objHTMLVideoElement{obj}

	} else {

		return &objHTMLVideoElement{newHTMLMediaElement(base)}
	}
}

// UniqueHTMLVideoElementInterface
// make sure HTMLVideoElement is a unique interface type
func (o *objHTMLVideoElement) UniqueHTMLVideoElementInterface() {}
