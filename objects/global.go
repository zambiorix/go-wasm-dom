package objects

import (

	// standard

	"syscall/js"
)

// singletons
//
var (

	// singletons

	global *Global

	jsObject js.Value
)

// init
//
func init() {

	jsObject = js.Global().Get("Object")
}

// Global ...
//
type Global struct {

	// publics

	Console Console

	Document Document

	Window Window
}

// NewGlobal ...
//
func NewGlobal() *Global {

	if global == nil {

		global = &Global{}

		global.Console = newConsole(js.Global().Get("console"))

		global.Document = newDocument(js.Global().Get("document"))

		global.Window = newWindow(js.Global().Get("window"))

		// TODO Screen

		// TODO Location

		// TODO History

		// TODO Navigator

		// TODO LocalStorage

		// TODO SessionStorage
	}

	return global
}
