package objects

import (

	// standard

	"syscall/js"
)

// JSValue
//
type JSValue interface {

	// publics

	Bool() bool

	Call(m string, args ...interface{}) js.Value

	Float() float64

	Get(p string) js.Value

	Index(i int) js.Value

	InstanceOf(t js.Value) bool

	Int() int

	Invoke(args ...interface{}) js.Value

	Length() int

	New(args ...interface{}) js.Value

	Set(p string, x interface{})

	SetIndex(i int, x interface{})

	String() string

	Type() js.Type
}
