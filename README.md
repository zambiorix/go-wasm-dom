[![GoDoc](http://godoc.org/gitlab.com/zambiorix/go-wasm-dom?status.svg)](http://godoc.org/gitlab.com/zambiorix/go-wasm-dom)

# go-wasm-dom

Golang WASM DOM implementation with example application

WARNING: experimental!

To install:

```
go get -u "gitlab.com/zambiorix/go-wasm-dom"
```

To initialize:

```
cd "${GOPATH}/src/gitlab.com/zambiorix/go-wasm-dom/application/app-test"

npm update --save-dev

npm run generate
```

To build:

```
npm run build
```

To run (with live update):

```
npm start
```

Open your browser at:

http://localhost:8080

Resources:

https://developer.mozilla.org/en-US/

https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model

https://developer.mozilla.org/en-US/docs/Web/HTML


https://tip.golang.org/pkg/syscall/js/

https://pspdfkit.com/blog/2018/optimize-webassembly-startup-performance/

http://krasimirtsonev.com/blog/article/deep-dive-into-client-side-routing-navigo-pushstate-hash

https://github.com/json-editor/json-editor
