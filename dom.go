package dom

//go:generate go run ./code-generation/generate.go

import "gitlab.com/zambiorix/go-wasm-dom/objects"

// Global ...
//
var Global *objects.Global

// init
//
func init() {

	Global = objects.NewGlobal()
}
